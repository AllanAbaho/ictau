<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$licenseUrl = \Yii::$app->urlManager->createAbsoluteUrl(['download/index','file'=>$license->file]);
?>
<div class="password-reset">
    <p>Hello <?= $license->user->name ?>,</p>

    <p>You have successfully acquired a license with ICTAU </p>

    <p>Please download your license below</p>
    <p><?= Html::a(Html::encode($licenseUrl), $licenseUrl) ?></p>
    <p>Disclaimer: This is an auto-generated mail. Please do not reply to it.</p>
    <p>
        If you have questions about this mailing, or need assistance, please direct your inquiries to 
        the  Contact Center. Replies to this message do not reach ICTAU.<br>
        Thank you, <br>
        ICTAU.
    <p>
    <p>If you'd like to unsubscribe and stop receiving these emails <a href="<?="%sendgrid_unsub_url%"?>">Click to unsubscribe.</a></p>
</div>
