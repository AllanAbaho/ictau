<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$licenseUrl = \Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hello <?= $user->name ?>,</p>

    <p>Thank you for being a member and supporting the ICT Association of Uganda</p>

    <p>An account has been created for you successfully.</p>

    <p>Login Details<br>
        Email: <?= $user->email ?><br>
        Password: <?= 'Ictau@123'; ?>
    </p>

    <p>This is a temporary password and you are advised to change it after logging in.</p>

    <p>Please login by clicking the link below</p>
    <p><?= Html::a(Html::encode($licenseUrl), $licenseUrl) ?></p>
    <p>Disclaimer: This is an auto-generated mail. Please do not reply to it.</p>
    <p>
        If you have questions about this mailing, or need assistance, please direct your inquiries to 
        the  Contact Center. Replies to this message do not reach ICTAU.<br>
        Thank you, <br>
        ICTAU.
    <p>
    <p>If you'd like to unsubscribe and stop receiving these emails <a href="<?="%sendgrid_unsub_url%"?>">Click to unsubscribe.</a></p>
</div>
