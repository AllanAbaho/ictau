<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$licenseUrl = \Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hello <?= $license->user->name ?>,</p>

    <p>Thank you for being a member and supporting the ICT Association of Uganda</p>

    <p>However, we would like to inform you that your membership expired on <b><?= $license->end_date; ?></b></p>

    <p>Please login to renew your membership below</p>
    <p><?= Html::a(Html::encode($licenseUrl), $licenseUrl) ?></p>

    <p>Kindly note that we have 3 membership Tiers listed below;</p> 
    <ol>
        <li>Students - UGX 5,000</li>
        <li>Individual Corporate - UGX 200,000</li>
        <li>Private Companies/Gov't MDAs/NGOs - UGX 2 million.</li>
    </ol> 

    <p>Membership is payment via the bank and the receipt uploaded in the system. Below are the bank details;</p> 

    <ol>
        <li>Bank Name: Housing Finance Bank</li>
        <li>Account Name: ICT Association of Uganda</li>
        <li>Account No: 0400053823</li>
    </ol> 

    <p>Disclaimer: This is an auto-generated mail. Please do not reply to it.</p>
    <p>
        If you have questions about this mailing, or need assistance, please direct your inquiries to 
        secretariat@ictau.ug. Replies to this message do not reach ICTAU.<br>
        Thank you, <br>
        ICTAU.
    <p>
    <p>If you'd like to unsubscribe and stop receiving these emails <a href="<?="%sendgrid_unsub_url%"?>">Click to unsubscribe.</a></p>
</div>
