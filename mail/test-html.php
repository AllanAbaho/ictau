<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<div class="password-reset">

    <p>You have successfully acquired a license with ICTAU </p>

    <p>Please download your license below</p>
    <p>Disclaimer: This is an auto-generated mail. Please do not reply to it.</p>
    <p>
        If you have questions about this mailing, or need assistance, please direct your inquiries to 
        the  Contact Center. Replies to this message do not reach ICTAU.<br>
        Thank you, <br>
        ICTAU.
    <p>
</div>
