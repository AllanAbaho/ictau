<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Ticket Verification';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-verify">
    <h1><?= Html::encode($this->title) ?></h1>
    <h5><b>Event Name: </b><?= $model->event->name;?></h5>
    <h5><b>Event Venue: </b><?= $model->event->venue;?></h5>
    <h5><b>Event Fee: </b><?= $model->event->cost ? $model->event->cost : $model->event->members_cost?></h5>
    <h5><b>Start Date: </b><?= $model->event->start_date;?></h5>
    <h5><b>End Date: </b><?= $model->event->end_date;?></h5>
    <h5><b>Start Time: </b><?= $model->event->start_time;?></h5>
    <h5><b>Closing Time: </b><?= $model->event->end_date;?></h5>
</div>
