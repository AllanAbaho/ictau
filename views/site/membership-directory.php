<?php

use yii\helpers\Html;
use yii\grid\GridView;
$logopath = Yii::$app->request->baseUrl;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Membership Directory';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
     <?php //print_r($dataProvider->models); ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <section class="area-members site-sec">
        <div class="club-search">

            <?php $form = ActiveForm::begin([
                'action' => ['membership-directory'],
                'method' => 'get',
                'options' => ['data-pjax' => true ]
            ]); ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'q')->textInput(['placeholder'=>"Enter Name",'autofocus'=>false])->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-md btn-danger']) ?>
                    </div>
                </div>
            </div><br>
            <?php ActiveForm::end(); ?>
        </div>

        <div class="site-sec-wrap">
            <ul class="list">
                <?php foreach ($dataProvider->models as $item):?>
                <li class="list-item list-item-img-txt-below">
                    <div class="img img-cover">
                    <?php    
                        if($item->user->photo){
                            echo Html::img('@web/photos/'.$item->user->photo);
                        }else{
                            echo Html::img('@web/photos/default.png');
                        }
                    ?>
                    </div>
                    <div class="txt">
                        <span class="category"><?php echo $item->application->membership->name; ?></span>
                        <h4 class="title"><?php echo $item->user->name ; ?></h4>
                        <p class="excerpt">
                            <?php 
                                if($item->user->brief_bio){
                                    echo $item->user->brief_bio; 
                                }else{
                                    echo "Lorem ipsum dolor sit amet consectetur adipisicing elit. Libero, labore?";
                                }
                            ?>
                        </p>
                        <?php if($item->user->website):?>
                        Website: <?= Html::a('Click Here',$item->user->website,['target'=>'_blank']); ?>
                        <?php endif; ?>
                    </div>
                </li>
                <?php endforeach; ?>

            </ul>
            
            <div class="row">
                <div class="col-md-12">
            <?php  echo LinkPager::widget([
                'pagination' => $dataProvider->pagination,
            ]) ?>
                </div>
            </div>
            
        </div>
    </section>

</div>
