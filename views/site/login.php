<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <!-- <!?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <!?= Yii::$app->session->getFlash('success') ?>
        </div>
    <!?php endif; ?> -->

<br><br>

<div class="col-lg-6 offset-lg-3 px-md-5 py-3 py-md-5 bg-white">

<h1 class="mb-3">Login</h1>
    <span>Create your account here</span>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-11\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-6">
                <?= Html::submitButton('Login', ['class' => 'btn btn-danger', 'name' => 'login-button']) ?>
            </div>
            <div class="col-md-6">
                <?= Html::a('Forgotten Password?', ['/user/request-password-reset']);?>
            </div>
        </div>

        Have no account yet? <?= Html::a('Register Here', ['register']);?>
        Forgotten Password? <?= Html::a('R Here', ['register']);?>



    <?php ActiveForm::end(); ?>
    <br>
</div>
</div>
