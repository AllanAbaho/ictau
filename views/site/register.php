<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create Account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
<br><br>

<div class="col-lg-6 offset-lg-3 px-md-5 py-3 py-md-5 bg-white">

    <div class="card-body">

    <h1 class="mb-3">Register</h1>
    <span>Create your account here</span>


        <?php $form = ActiveForm::begin([
            // 'id' => 'login-form',
            // // 'layout' => 'horizontal',
            // 'fieldConfig' => [
            //     'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            //     'labelOptions' => ['class' => 'col-lg-12'],
            // ],
        ]); ?>

        <div class="row">
            <div class="col-md-6">        
                <?= $form->field($model, 'first_name')->textInput(['placeholder'=>'First Name','required'=>true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'last_name')->textInput(['placeholder'=>'Last Name','required'=>true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email Address','required'=>true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'primary_phone_number')->textInput(['placeholder'=>'Phone Number','required'=>true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password','required'=>true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'password_confirm')->passwordInput(['placeholder'=>'Confirm Password','required'=>true]) ?>
            </div>
        </div>        

        <div class="form-group">
            <?= Html::submitButton('Create Account', ['class' => 'btn btn-danger', 'name' => 'login-button']) ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a('Register Organisation Here','register-organisation'); ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
    <br>
    </div>
</div>
</div>
