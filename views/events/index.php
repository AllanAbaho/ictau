<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">

    <div class="row pt-5 g-5">
                    <div class="offset-md-2 col-md-8">
                        <div class="row gap-3">
                            <div class="card">
                                <div class="card-header bg-white">
                                    <h4 class="mb-0">Events</h4>
                                </div>
                                <div class="card-body">
                                <?php if (Yii::$app->session->hasFlash('success')): ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <?= Yii::$app->session->getFlash('success') ?>
                                    </div>
                                <?php endif; ?>
                                    <div class="d-grid gap-3">
                                        <?php foreach($dataProvider->models as $item ): ?>
                                        <div class="pb-2 border-bottom">
                                            <h5><?= $item['name'] ?></h5>
                                            <p><?= $item['description'] ?></p>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <?= Html::a( 'Details', ['/events/view','id' =>$item['id']], ['class' => 'text-danger']) ?>
                                                <time class="h6 mb-0"><?=  date("F jS, Y", strtotime($item['start_date'])) ?></time>
                                            </div>
                                        </div>
                                        <img src="<?= Yii::getAlias('@web');?>/events/<?= $item['file']?>" width="300px" class="img-responsive" />
                                        <?php endforeach; ?>
                                  
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

   


</div>
