<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Events */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="events-view">

    <!-- <p>
        <!?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <!?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->
<div class="row">
    <div class="col-md-8">
    <h1 class="mt-3 mb-3"><?= Html::encode($this->title) ?></h1>
                            <div class="card">
                                <!-- <div class="card-header pt-3 d-flex flex-wrap justify-content-start bg-white">
                                    <div class="py-2 px-3 me-2 bg-dark text-white rounded">
                                        <h6 class="border-bottom pb-2">Fee - Guest</h6>
                                        <span class="d-block"><?php if(!empty($model->cost)){  echo number_format($model->cost); } else{ echo '0'; } ?> UGX</span>
                                    </div>
                                    <div class="py-2 px-3 me-2 bg-success text-white rounded">
                                        <h6 class="border-bottom pb-2">Fee - Member</h6>
                                        <span class="d-block"><?php if(!empty($model->members_cost)){ echo  number_format($model->members_cost); } else{ echo '0';} ?> UGX</span>
                                    </div>
                                    <div class="py-2 px-3 me-2 bg-danger text-white rounded">
                                        <h6 class="border-bottom pb-2">Date</h6>
                                        <span class="d-block"><?=  date("F jS, Y", strtotime($model->start_date)) ?></span>
                                    </div>
                                    <div class="py-2 px-3 me-2 bg-dark text-white rounded">
                                        <h6 class="border-bottom pb-2">Time</h6>
                                        <span class="d-block"><?=  $model->start_time.' - '.$model->end_time ?></span>
                                    </div>
                                    <div class="py-2 px-3 me-2 bg-danger text-white rounded">
                                        <h6 class="border-bottom pb-2">Venue</h6>
                                        <span class="d-block"><?=  $model->venue ?></span>
                                    </div>
                                    <div class="py-2 px-3 me-2 bg-success text-white rounded">
                                        <h6 class="border-bottom pb-2">CPD Points</h6>
                                        <span class="d-block"><?php echo $model->cpd_points; ?> </span>
                                    </div>
                                </div> -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?= DetailView::widget([
                                                'model' => $model,
                                                'attributes' => [
                                                    'name', 'cost', 'start_time', 'end_time', 'venue','members_cost','start_date', 'end_date', 
                                                ],
                                            ]) ?>
                                        </div>
                                        <div class="col-md-6">
                                            <img src="<?= Yii::getAlias('@web');?>/events/<?= $model->file;?>" class="img-responsive" />
                                        </div>
                                    </div>
                                    <p><?=  $model->description ?></p>
                                </div>
                            </div>
    </div>
    <?php if($model->start_date >= date('Y-m-d')): ?>
    <div class="col-md-4">
        <h1>Register for Event</h1>
        <div class="event-payments-form">
        <div class="card-body-inner">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($payment, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($payment, 'phone_number')->textInput(['maxlength' => true]) ?>

            <?= $form->field($payment, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($payment, 'event_id')->hiddenInput(['value' => $model->id])->label(false) ?>

            <?= $form->field($payment, 'amount')->hiddenInput(['value' => $model->cost])->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        </div>
    </div>
    <?php endif; ?>
</div>
    

</div>
