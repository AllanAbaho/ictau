<?php
use yii\helpers\Html;
use app\assets\DashboardAsset;
DashboardAsset::register($this);
$this->beginPage();
$logopath = Yii::$app->request->baseUrl;

?>
<!--
=========================================================
 Yii2 Framework Material Dashboard - v1.0.0
=========================================================

 Product Page: https://www.coderseden.com/product/material-dashboard-yii2
 Copyright 2020 CodersEden (https://www.coderseden.com)
 Licensed under MIT (https://opensource.org/licenses/MIT)

 Developed by CodersEden

 =========================================================
 Material Dashboard - v2.1.2
 =========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2020 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

 =========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Extra details for Demo -->
    <meta name="theme-color" content="#ffffff">
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => $logopath.'/images/favicon.png']); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="<?= \Yii::getAlias('@web/dashboard/css/material-dashboard.css?v=2.1.2'); ?>" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?= \Yii::getAlias('@web/dashboard/demo/demo.css'); ?>" rel="stylesheet" />
    <link href="<?= \Yii::getAlias('@web/dashboard/css/custom.css'); ?>" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css"  rel="stylesheet" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="">
<!-- Extra Body details for Demo -->
<?php $this->beginBody() ?>
<div class="wrapper">

<?= $this->render(
	'left.php'
)
?>
    <div class="main-panel">
<?= $this->render(
	'header.php'
)
?>

<?= $this->render(
	'content.php',
	['content' => $content]
) ?>

<?= $this->render(
	'footer.php'
)
?>
    </div>
</div>

<!-- <!?= $this->render(
	'plugin.php'
)
?> -->
<!--   Core JS Files   -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/core/jquery.min.js');?>"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/core/popper.min.js');?>"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/core/bootstrap-material-design.min.js');?>"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/perfect-scrollbar.jquery.min.js');?>"></script>
<!-- Plugin for the momentJs  -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/moment.min.js');?>"></script>
<!--  Plugin for Sweet Alert -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/sweetalert2.js');?>"></script>
<!-- Forms Validations Plugin -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/jquery.validate.min.js');?>"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/jquery.bootstrap-wizard.js');?>"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/bootstrap-selectpicker.js');?>"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/bootstrap-datetimepicker.min.js');?>"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/jquery.dataTables.min.js');?>"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/bootstrap-tagsinput.js');?>"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/jasny-bootstrap.min.js');?>"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/fullcalendar.min.js');?>"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/jquery-jvectormap.js');?>"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/nouislider.min.js');?>"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/core.js');?>"></script>
<!-- Library for adding dinamically elements -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/arrive.min.js');?>"></script>
<!-- Chartist JS -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/chartist.min.js');?>"></script>
<!--  Notifications Plugin    -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugins/bootstrap-notify.js');?>"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/material-dashboard.js?v=2.1.2');?>" type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?= \Yii::getAlias('@web/dashboard/demo/demo.js');?>"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/plugin.js');?>" type="text/javascript"></script>

  <!-- My Export PDF Excel Functioanlity -->
  <script src="<?= \Yii::getAlias('@web/dashboard/js/myDataTable.js');?>" type="text/javascript"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/pdfmake.min.js');?>"></script>
  <script src="<?= \Yii::getAlias('@web/dashboard/js/vfs_fonts.js');?>"></script>

  <script src="<?= \Yii::getAlias('@web/js/material-datepicker.js');?>" type="text/javascript"></script>
  <script src="<?= \Yii::getAlias('@web/js/mydatepicker.js');?>" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js" type="text/javascript"></script>

  <script>
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();
    });
  </script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
