<div class="sidebar" data-color="green" data-background-color="white" data-image="<?= \Yii::getAlias('@web/dashboard/img/sidebar-1.jpg'); ?>">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="#" class="simple-text logo-normal">
          <img width="190px" src="<?= \Yii::getAlias('@web/images/ictau-logo.png'); ?>" alt="">
        </a></div>
      <div class="sidebar-wrapper">
      <?php 
  $directoryURI = $_SERVER['REQUEST_URI'];
  $path = parse_url($directoryURI, PHP_URL_PATH);
  $components = explode('/', $path);
  $last_part = end($components);
?>

        <ul class="nav">
          <?php if(\Yii::$app->user->identity->role == 'Admin'): ?>
          <li class="<?php if ($last_part=="default") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/default']);?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <!-- <li class="<?php if ($last_part=="membership-report") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/membership-report']);?>">
              <i class="material-icons">content_paste</i>
              <p>Membership Management</p>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#yii2Example" aria-expanded="true">
              <i class="material-icons">summarize</i>
              <p>Membership
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="yii2Example">
              <ul class="nav">
              <li class="<?php if ($last_part=="corporate-report") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/corporate-report']);?>">
                    <span class="sidebar-mini"> LM </span>
                    <span class="sidebar-normal"> List Companies </span>
                  </a>
                </li>
                <li class="<?php if ($last_part=="membership-report") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/membership-report']);?>">
                    <span class="sidebar-mini"> LM </span>
                    <span class="sidebar-normal"> List Members </span>
                  </a>
                </li>
                <li class="<?php if ($last_part=="create") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/users/create']);?>">
                    <span class="sidebar-mini"> AM </span>
                    <span class="sidebar-normal"> Add Member </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>          
          <li class="<?php if ($last_part=="applications") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/applications']);?>">
              <i class="material-icons">build</i>
              <p>Applications</p>
            </a>
          </li>
          <li class="<?php if ($last_part=="events") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/events']);?>">
              <i class="material-icons">memory</i>
              <p>Events</p>
            </a>
          </li>
          <li class="<?php if ($last_part=="membership-types") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/membership-types']);?>">
              <i class="material-icons">library_books</i>
              <p>Membership Types</p>
            </a>
          </li>
          <li class="<?php if ($last_part=="occupations") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/occupations']);?>">
              <i class="material-icons">content_paste</i>
              <p>Occupations</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#yii3Example" aria-expanded="true">
              <i class="material-icons">money</i>
              <p>Payments Section
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="yii3Example">
              <ul class="nav">
                <li class="<?php if ($last_part=="membership-payments") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/membership-payments']);?>">
                    <span class="sidebar-mini"> MP </span>
                    <span class="sidebar-normal"> Membership Payments </span>
                  </a>
                </li>
                <li class="<?php if ($last_part=="event-payments-members") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/event-payments-members']);?>">
                    <span class="sidebar-mini"> EPM </span>
                    <span class="sidebar-normal"> Events Payments - Members </span>
                  </a>
                </li>
                <li class="<?php if ($last_part=="event-payments-guests") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/event-payments-guests']);?>">
                    <span class="sidebar-mini"> EPG </span>
                    <span class="sidebar-normal"> Events Payments - Guests </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#yii2Example" aria-expanded="true">
              <i class="material-icons">summarize</i>
              <p>Reports
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse show" id="yii2Example">
              <ul class="nav">
                <li class="<?php if ($last_part=="upcoming-events") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/upcoming-events']);?>">
                    <span class="sidebar-mini"> UE </span>
                    <span class="sidebar-normal"> Upcoming Events </span>
                  </a>
                </li>
                <li class="<?php if ($last_part=="past-events") {echo "nav-item active"; } else  {echo "nav-item";}?>">
                  <a class="nav-link" href="<?=\yii\helpers\Url::to(['/admin/reports/past-events']);?>">
                    <span class="sidebar-mini"> PE </span>
                    <span class="sidebar-normal"> Past Events </span>
                  </a>
                </li>
              </ul>
            </div>
          </li>          
          <?php else: ?>
          <li class="<?php if ($last_part=="dash") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/profile/dash']);?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="<?php if ($last_part=="profile") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/profile']);?>">
              <i class="material-icons">content_paste</i>
              <p>My Profile</p>
            </a>
          </li>       
          <li class="<?php if ($last_part=="events") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/events']);?>">
              <i class="material-icons">build</i>
              <p>Upcoming Events</p>
            </a>
          </li>       
          <li class="<?php if ($last_part=="past-events") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/events/past-events']);?>">
              <i class="material-icons">build</i>
              <p>Past Events</p>
            </a>
          </li>       
          <li class="<?php if ($last_part=="applications") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/applications']);?>">
              <i class="material-icons">memory</i>
              <p>My Applications</p>
            </a>
          </li>       
          <li class="<?php if ($last_part=="licenses") {echo "nav-item active"; } else  {echo "nav-item";}?>">
            <a class="nav-link" href="<?=\yii\helpers\Url::to(['/member/licenses']);?>">
              <i class="material-icons">library_books</i>
              <p>My Certificates</p>
            </a>
          </li>       
          <?php endif; ?>
        </ul>
      </div>
    </div>