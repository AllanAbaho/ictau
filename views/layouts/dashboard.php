<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$logopath = Yii::$app->request->baseUrl;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html> 
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => $logopath.'/images/favicon.png']); ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    if( Yii::$app->user->isGuest){
        return Yii::$app->getResponse()->redirect(['/site/login']);
    }
    $role = Yii::$app->user->identity->role;
    NavBar::begin([
        'brandLabel' => '<img src="'.$logopath.'/images/ictau-logo.png" class="pull-left"/>',
        'brandUrl' => '',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Dashboard', 'url' => ['/admin/default'], 'visible'=>$role=='Admin'],
            ['label' => 'Members', 'url' => ['/admin/users'], 'visible'=>$role=='Admin'],
            ['label' => 'Dashboard', 'url' => ['/member/profile/dash'], 'visible'=>$role=='Member'],
            ['label' => 'Profile', 'url' => ['/member/profile'], 'visible'=>$role=='Member'],
            ['label' => 'Upcoming Events', 'url' => ['/member/events'], 'visible'=>$role=='Member'],
            ['label' => 'Applications', 'url' => ['/member/applications'], 'visible'=>$role=='Member'],
            ['label' => 'Applications', 'url' => ['/admin/applications'], 'visible'=>$role=='Admin'],
            // ['label' => 'Certificates', 'url' => ['/admin/licenses'], 'visible'=>$role=='Admin'],
            ['label' => 'Certificates', 'url' => ['/member/licenses'], 'visible'=>$role=='Member'],
            ['label' => 'Events', 'url' => ['/admin/events'], 'visible'=>$role=='Admin'],
            ['label' => 'Membership Types', 'url' => ['/admin/membership-types'], 'visible'=>$role=='Admin'],
            ['label' => 'Error Logs', 'url' => ['/super/logs'], 'visible'=>$role=='Super'],
            Yii::$app->user->isGuest ?  : (
                '<li>
                </li>
                <li class="nav-item dropdown ms-4">
                <a class="nav-link dropdown-toggle p-0 img-cover" href="#" id="navbarScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="'.Yii::getAlias('@web').'/photos/'.Yii::$app->user->identity->photo.'" alt="" width="40" height="40" style="border-radius: 100%;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-caret-down-fill ms-1" viewBox="0 0 16 16"><path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/></svg>
                </a>
                <ul class="dropdown-menu dropdown-menu-end mt-3 py-1" aria-labelledby="navbarScrollingDropdown">
                    <li>'.
                        Html::a( Yii::$app->user->identity->first_name.' '. Yii::$app->user->identity->last_name, ['/member/profile'], ['class' => 'dropdown-item']) 
                    .'</li>
                    <li>'.
                       
                        Html::a( 'Edit Profile', ['/member/profile/update','id' =>Yii::$app->user->identity->id], ['class' => 'dropdown-item']) 
                    .'</li>
                    <li>
                        <a class="dropdown-item" href="#">Help</a>
                    </li>
                    <li>
                        <hr class="dropdown-divider my-1">
                    </li>'.
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->first_name . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    .
                    '</li>
                </ul>
            </li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ICTAU <?= date('Y') ?>. All Rights Reserved.</p>

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
