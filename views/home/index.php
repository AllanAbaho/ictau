<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use yii\helpers\Url;


/* @var $this yii\web\View */
$logopath = Yii::$app->request->baseUrl;

$this->title = 'ICTAU';
?>
<div class="site-index">

<div class="row-home">
    <div class="d-none d-lg-block col-lg-6 p-0 img img-cover">
<img  src="<?php echo $logopath; ?>/images/o.jpg"/>
    </div>
</div>
<div  class="col-lg-6 px-md-5 py-3 py-md-5">
<?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

<h1 class="mt-5 mb-3">Login</h1>
<span>Sign in to start your session</span>



        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            // 'action' => Url::to(['/site/login']),
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-11\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-6">
                <?= Html::submitButton('Login', ['class' => 'btn btn-danger', 'name' => 'login-button']) ?>
            </div>
           
        </div>
        Forgotten Password? <?= Html::a('Reset Password', ['site/request-password-reset']);?><br>
        Have no account yet? <?= Html::a('Register Here', ['site/register']);?>



    <?php ActiveForm::end(); ?>
    <br>
</div>
</div>
