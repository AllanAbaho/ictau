<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    public $layout = 'main';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         'logout' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionVerifyTicket($token)
    {
        $model = \app\models\EventPayments::findOne(base64_decode($token));
        if($model === null){
            throw new NotFoundHttpException('The requested Ticket does not exist.');
        }
        return $this->render('verify-ticket',[
            'model'=>$model,
        ]);
        // userId=licence->userId and category=registration
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest){
            $role = Yii::$app->user->identity->role;
            if($role =='Member' || $role=='Organisation'){
                return $this->redirect(['/member/profile/dash']);
            }elseif($role =='Admin'){
                return $this->redirect(['/admin/default']);
            }elseif($role =='Super'){
                return $this->redirect(['/super/logs']);
            }
        }
        return $this->render('index');
    }


    public function actionMembershipDirectory()
    {
        $licenses = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Licenses::find()->where(['>=','end_date',date('Y-m-d')]),
        ]);

        $searchModel = new \app\models\DirectorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('membership-directory',[
            'dataProvider' => $dataProvider,
            'model' => $searchModel
        ]);
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $model = new \app\models\Users;
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $model->role = 'Member';
            if($model->save()){
                $notification = new \app\models\Notifications;
                $admin = \app\models\Users::findOne(['role'=>'Admin']);
                $notification->user_id = $admin->id;
                $notification->subject = 'New Account Created';
                $message = 'Account created successfully.';
                $notification->message = $message;
                $notification->save();
                \Yii::$app->mailer->compose(
                    ['html' => 'registration-html'],
                    [
                        'user' => $model,
                    ]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                ->setTo($model->email)
                ->setSubject('Confirmation Email')
                ->send();

                Yii::$app->session->setFlash('success',$message);
                return $this->redirect(['/home/index']);        
            }
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionRegisterOrganisation()
    {
        $model = new \app\models\Users;
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $model->role = 'Organisation';
            $model->first_name = '';
            $model->last_name = '';
            if($model->save()){
                $notification = new \app\models\Notifications;
                $admin = \app\models\Users::findOne(['role'=>'Admin']);
                $notification->user_id = $admin->id;
                $notification->subject = 'New Account Created';
                $message = 'Account created successfully.';
                $notification->message = $message;
                $notification->save();
                \Yii::$app->mailer->compose(
                    ['html' => 'registration-html'],
                    [
                        'user' => $model,
                    ]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                ->setTo($model->email)
                ->setSubject('Confirmation Email')
                ->send();

                Yii::$app->session->setFlash('success',$message);
                return $this->redirect(['/home/index']);        
            }
        }
        return $this->render('register-organisation', [
            'model' => $model,
        ]);
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

        /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error',$e->getMessage());
            return $this->redirect(['login']);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved successfully.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }


}
