<?php

namespace app\controllers;

use Yii;
use app\models\Events;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Events::find()->orderBy(['start_date' => SORT_DESC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $payment = new \app\models\GuestPayments;
        if ($payment->load(Yii::$app->request->post())) {
            if($payment->amount <= 0){
                $payment->status = 'Free';
                $payment->save();
                Yii::$app->session->setFlash('success','Please check your email to download the ticket!');
                return $this->redirect(['index']);
            }else{
                $payment->status = 'Pending';
                return $this->redirect(['olycash', 'event_id' => $model->id,'payment_id' => $payment->id,]);
            }
        }

        return $this->render('view', [
            'model' => $model,
            'payment' => $payment
        ]);
    }

    public function actionOlycash($event_id, $payment_id)
    {
        $event = \app\models\Events::findOne($event_id);
        $payment =  \app\models\GuestPayments::findOne($payment_id);

        return $this->render('olycash', [
            'event' => $event,
            'payment' => $payment,
        ]);
    }

    public function actionConfirm($id){
        $payment =  \app\models\GuestPayments::findOne($id);
        $purchase_id = $_POST['purchase_id'];
        $message = $_POST['message'];
        $payment_type = $_POST['payment_type'];
        if($message == 'success'){
            try {
                $payment->reference_number = $purchase_id;
                $payment->payment_type ='Olycash';
                $payment->status = 'Paid';
                if($payment->save()){
                    Yii::$app->session->setFlash('success','Please check your email to download the ticket!');
                    return $this->redirect(['/events/index']);
                }else{
                    print_r($payment->getFirstErrors());
                }
            } catch (IntegrityException $e) {
                echo $e->getMessage();
            } catch (NotFoundHttpException $e) {
                echo $e->getMessage();
            }
            return $this->redirect(['/events/index']);
        }

    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Events();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
