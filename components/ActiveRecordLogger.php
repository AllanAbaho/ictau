<?php
 namespace app\components;
 use yii\base\Behavior;
 use yii\db\ActiveRecord;
 use app\models\ActiveRecordLog;
 use yii\db\Expression;
 use yii\helpers\StringHelper;
 use Yii;

 /**
 * Created by PhpStorm.
 * User: yusuf
 * Date: 08/12/2018
 * Time: 01:42
 */
class ActiveRecordLogger extends Behavior
{
    private  $filter = ['updated_at','created_at'];
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_REFRESH => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE =>'afterDelete',
        ];
    }

    public function afterInsert($event)
    {
        $name = Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->email;
        $log = new ActiveRecordLog();
        $log->description = "$name Added ".StringHelper::basename(get_class($this->owner));
        $log->action ='CREATE';
        $log->model = $this->owner->className();
        $log->model_id = $this->owner->primaryKey;
        $log->created_at = new Expression('NOW()');
        $log->userid = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
        $log->field = json_encode($this->owner->attributes);
        $log->save(false);

    }

    public function afterUpdate($event)
    {

        $name = Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->email;
        $log = new ActiveRecordLog();
        $log->description = "$name Updated ".StringHelper::basename(get_class($this->owner))."[".$this->owner->primaryKey."]";
        $log->action ='UPDATE';
        $log->model = $this->owner->className();
        $log->model_id = $this->owner->primaryKey;
        $log->created_at = new Expression('NOW()');
        $log->userid = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
        //build array of changed attribute
        $changedItems =[];
        foreach ($event->changedAttributes as $key => $item){
            if(!in_array($key,$this->filter))
                $changedItems[$key] = $this->owner->attributes[$key];
        }
        $log->field = json_encode($changedItems);
        $log->save(false);
    }

    public function afterDelete($event)
    {
        $name = Yii::$app->user->isGuest ? 'Guest' : Yii::$app->user->identity->email;
        $log = new ActiveRecordLog();
        $log->description = "$name Delete ".$this->owner->className()."[".$this->owner->primaryKey."]";
        $log->action ='DELETE';
        $log->model = $this->owner->className();
        $log->model_id = $this->owner->primaryKey;
        $log->created_at = new Expression('NOW()');
        $log->userid = !Yii::$app->user->isGuest ? Yii::$app->user->id : null;
        $log->save(false);
    }
}