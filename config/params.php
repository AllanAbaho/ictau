<?php

return [
    'adminEmail' => 'secretariat@ictau.ug',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
