<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Applications;
use app\models\Users;
use yii\console\Controller;
use yii\console\ExitCode;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionIctauMembers(){       

        $items = \app\models\IctauMembers::find()->all();
        $membership_type = \app\models\MembershipTypes::find()->where(['like','name','%corporate%',false])->limit(1)->one();
        $i=0;
        foreach ($items as $item) {
            $transaction = \Yii::$app->db->beginTransaction();
            $i++;
            echo "\nimporting ".$item->name."\n";
            // $array = explode(' ',$item->name,2);
            // $firstname = $array[0];
            // $lastname = $array[1];
            try {
                //register user info
                $user = new \app\models\Users();
                $user->first_name = '';
                $user->last_name = '';
                $user->organisation = $item->name;
                $user->password = 'Ictau@123';
                $user->email = $item->email_address;
                $user->primary_phone_number = $item->primary_phone_number;
                $user->role = 'Organisation';
                $user->status = '10';
                if ($user->save()) {
                    echo "-- saving application\n";
                    $application = new \app\models\Applications();
                    $application->user_id = $user->id;
                    $application->membership_type_id = $membership_type->id;
                    $application->status = 'Complete';
                    if($application->save()){
                        echo "-- saving License\n";
                        $license = new \app\models\Licenses;
                        $license->user_id = $user->id;
                        $license->application_id = $application->id;
                        $license->end_date = $item->license_end_date ? $item->license_end_date : '2020-12-31';
                        $license->save();    
                    }
                    $item->delete();
                }else{
                    echo "Error adding User\n";
                    print_r($user->getFirstErrors());
                    throw new IntegrityException('Price:' . join(', ', $user->getFirstErrors()));
                }
                echo "Commiting now\n";
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                echo $e->getMessage();
            }
        }
        echo $i;
    }

    public function actionCreateAdmin(){
        $user = new \app\models\Users;
        $user->first_name = 'Ictau';
        $user->last_name = 'Admin';
        $user->email = 'secretariat@ictau.ug';
        $user->role = 'Admin';
        $user->primary_phone_number = '0700000000';
        $user->status = '10';
        $user->password = 'Admin@123';
        $user->password_confirm = $user->password;

        if($user->save()){
            echo "Admin successfully created \n";
        }else{
            $errors =  $user->getFirstErrors();
            foreach($errors as $error){
                echo $error;
            }
        }
    }

    public function actionLicenseRenewal(){
        $licenses = \app\models\Licenses::find()->where(['<=','end_date',date('Y-m-d')])->all();
        foreach($licenses as $license){
            if($license){
                $days_remaining = round((strtotime($license->end_date) - strtotime(date('Y-m-d')))/86400);
                if($days_remaining < 60){
                    if($license->user && $license->user->email){
                        \Yii::$app->mailer->compose(
                            ['html' => 'license-renewal-html'],
                            [
                                'license' => $license,
                            ]
                        )
                        ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                        ->setTo($license->user->email)
                        ->setSubject('Renew Your Membership')
                        ->send();    
                    }
                } 
            }
        }
    }

    public function actionLicenseExpired(){
        $licenses = \app\models\Licenses::find()->where(['<=','end_date',date('Y-m-d')])->all();
        foreach($licenses as $license){
            if($license){
                if($license->user && $license->user->email){
                    \Yii::$app->mailer->compose(
                        ['html' => 'license-expired-html'],
                        [
                            'license' => $license,
                        ]
                    )
                    ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                    ->setTo($license->user->email)
                    ->setSubject('Renew Your Membership')
                    ->send();    
                }
            }
        }
    }

    public function actionEmailTest(){
        \Yii::$app->mailer->compose(['html' => 'test-html'])
        ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU Membership Certificate'])
        ->setTo('abahoallans@gmail.com')
        ->setSubject('ICTAU Membership Certificate')
        ->send();
    }

    public function actionEventNotification(){
        $event = \app\models\Events::find()->where(['is_notified'=>null])->andWhere(['like','created_at',date('Y-m-d')])->limit(1)->one();
        $members = \app\models\Users::find()->where(['role'=>'Member'])->all();
        foreach($members as $member){
            $notification = new \app\models\Notifications;
            $notification->user_id = $member->id;
            $notification->subject = $event->name;
            $message = 'A new event has been added and please go to the Upcoming events tab to know how to attend if interested!';
            $notification->message = $message;
            $notification->save();
            if(!empty($member->email)){
                \Yii::$app->mailer->compose(
                    ['html' => 'event-html'],
                    [
                        'user' => $member,
                    ]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                ->setTo($member->email)
                ->setSubject('ICTAU '.$event->name)
                ->send();    
            }
        }
        $event->is_notified = 'Y';
        $event->save(false);
    }

    public function actionDeleteGibberish($from, $to){
        $users = Users::find()
        ->andFilterWhere(['between', 'id', $from, $to])->all();
        $count = 0;
        foreach($users as $user){
            echo "Lookin into User ".$user->id ."\n";
            \app\models\Applications::deleteAll(['user_id'=>$user->id]);
            \app\models\Payments::deleteAll(['user_id'=>$user->id]);
            \app\models\EventFeedback::deleteAll(['user_id'=>$user->id]);
            \app\models\EventPayments::deleteAll(['user_id'=>$user->id]);
            \app\models\Notifications::deleteAll(['user_id'=>$user->id]);
            \app\models\Licenses::deleteAll(['user_id'=>$user->id]);
            $user->delete();
            echo "Deleted Everythin about User ".$user->first_name ."\n";
            $count += 1;
        }
        echo $count ." Users Deleted Successfully";

    }
}
