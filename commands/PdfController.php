<?php

namespace app\commands;

use app\models\Licenses;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use Da\QrCode\QrCode;
use yii\helpers\Url;
use app\models\Applications;
use Yii;
use app\models\Payments;
use yii\data\ActiveDataProvider;
use yii\db\IntegrityException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


class PdfController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionConvert()
    {        
        $formatter = \Yii::$app->formatter;
        $licenses = Licenses::find()->where(['file'=>null])
        ->orderBy('updated_at desc')->limit(1)->all();
        foreach ($licenses as $license){
            if($license){
                //convert licence to pdf
                if(!$license->application->membership->template){
                    echo "No template for this product";
                    continue;
                }
                $html = $this->generateTemplate($license->application->membership->template, $this->getFields($license));
                echo $html;
                $dompdf = new \Dompdf\Dompdf(['enable_remote' => true]);
                $dompdf->load_html($html);
                $dompdf->setPaper('A4', 'landscape');
                $dompdf->render();
                $filename = 'license-'.uniqid().'.pdf';
                $output = $dompdf->output();
                file_put_contents(\Yii::getAlias('@app/web/licenses/').$filename, $output);
                $license->file = $filename;
                $license->save(false);
                // \Yii::$app->mailer->compose(
                //     ['html' => 'license-html'],
                //     [
                //         'license' => $license,
                //     ]
                // )
                // ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU Membership Certificate'])
                // ->setTo($license->user->email)
                // ->setSubject('ICTAU Membership Certificate')
                // ->send();
            }
        }
        return "Nothing To Convert";
        
    }


    public function generateTemplate($template, $replacement)
    {
        $templ = str_replace(array_keys($replacement), array_values($replacement), $template);
        return $templ;
    }

    public function getFields($license)
    {
        $formatter = \Yii::$app->formatter;
        // $verify_link = Url::to(['site/verify', 'token' => base64_encode($license->id)],true);
        // $qrcode =  $this->generateQrCode($license->id, $verify_link);
        $url = \Yii::$app->urlManager;
        $logo = $url->createAbsoluteUrl(['images/logo.png']);
        $banner = $url->createAbsoluteUrl(['images/banner.png']);
        $footer = $url->createAbsoluteUrl(['images/footer.png']);
        $viceChair = $url->createAbsoluteUrl(['images/vicechair.png']);
        $chair = $url->createAbsoluteUrl(['images/chair.png']);

        try {
            return [
                '_LOGO_' => $logo,
                '_BANNER_' => $banner,
                '_FOOTER_' => $footer,
                '_VICECHAIR_' => $viceChair,
                '_CHAIR_' => $chair,
                '_NAME_' => $license->user->name,
                '_MEMBERSHIPTYPE_' => $license->application->membership->name,
                '_LICENSEYEAR_' => $formatter->asDate($license->start_date, 'yy'),
                '_LICENSEID_' => $license->id,
                '_CALENDARYEAR_' => $formatter->asDate($license->start_date, 'Y') . '/'.$formatter->asDate($license->end_date, 'Y')
                // '_QR_CODE_' =>  $qrcode,
            ];
        } catch (Exception $e) {
            return [];
        }
        return [];
    }

    // public function generateQrCode($file, $text)
    // {
    //     $qrCode = (new QrCode($text))
    //         ->setSize(250)
    //         ->setMargin(5)
    //         ->useForegroundColor(0, 0, 0);

    //     $filename = \Yii::getAlias('@app').'/qr/qr_'.$file.'.png';
    //     $qrCode->writeFile($filename);
    //     return $filename;
    // }
}
