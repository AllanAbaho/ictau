<?php

namespace app\commands;

use app\models\EventPayments;
use app\models\GuestPayments;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use Da\QrCode\QrCode;
use yii\helpers\Url;
use app\models\Applications;
use Yii;
use app\models\Payments;
use yii\data\ActiveDataProvider;
use yii\db\IntegrityException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


class TicketController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionConvertMemberTickets()
    {
        
        $formatter = \Yii::$app->formatter;
        $payments = EventPayments::find()->where(['file'=>null])
        ->orderBy('created_at asc')->limit(2)->all();
        foreach ($payments as $payment){
            $template = \app\models\TicketTemplate::findOne(1);
            if($payment){
                if(!$template->body){
                    echo "No template found";
                    continue;
                }
                $html = $this->generateTemplate($template->body, $this->getFields($payment));
                echo $html;
                $dompdf = new \Dompdf\Dompdf(['enable_remote' => true]);
                $dompdf->load_html($html);
                $dompdf->setPaper('A4', 'landscape');
                $dompdf->render();
                $filename = 'ticket-'.uniqid().'.pdf';
                $output = $dompdf->output();
                file_put_contents(\Yii::getAlias('@app/web/tickets/').$filename, $output);
                $payment->file = $filename;
                $payment->save(false);
            }
        }
        return "Nothing To Convert";
    }

    public function actionConvertGuestTickets()
    {
        
        $formatter = \Yii::$app->formatter;
        $payments = GuestPayments::find()->where(['file'=>null])
        ->orderBy('created_at asc')->limit(2)->all();
        foreach ($payments as $payment){
            $template = \app\models\TicketTemplate::findOne(1);
            if($payment){
                if(!$template->body){
                    echo "No template found";
                    continue;
                }
                $html = $this->generateTemplate($template->body, $this->getFields($payment));
                echo $html;
                $dompdf = new \Dompdf\Dompdf(['enable_remote' => true]);
                $dompdf->load_html($html);
                $dompdf->setPaper('A4', 'landscape');
                $dompdf->render();
                $filename = 'ticket-'.uniqid().'.pdf';
                $output = $dompdf->output();
                file_put_contents(\Yii::getAlias('@app/web/tickets/').$filename, $output);
                $payment->file = $filename;
                $payment->save(false);
                \Yii::$app->mailer->compose(
                    ['html' => 'ticket-html'],
                    [
                        'payment' => $payment,
                    ]
                )
                ->setFrom([\Yii::$app->params['adminEmail'] => 'ICTAU'])
                ->setTo($payment->email)
                ->setSubject('ICTAU Event')
                ->send();
            }
        }
        return "Nothing To Convert";
    }

    public function generateTemplate($template, $replacement)
    {
        $templ = str_replace(array_keys($replacement), array_values($replacement), $template);
        return $templ;
    }

    public function getFields($ticket)
    {
        $formatter = \Yii::$app->formatter;
        $verify_link = Url::to(['site/verify-ticket', 'token' => base64_encode($ticket->id)],true);
        $code =  $this->generateQrCode($ticket->id, $verify_link);

        $url = \Yii::$app->urlManager;
        $logo = $url->createAbsoluteUrl(['images/logo.png']);
        $qrcode = $url->createAbsoluteUrl(['qr/qr_'.$ticket->id.'.png']);

        try {
            return [
                '_LOGO_' => $logo,
                '_QRCODE_' =>  $qrcode,
                '_EVENT_NAME_' => $ticket->event->name,
                '_VENUE_' => $ticket->event->venue,
                '_TICKET_ID_' => $ticket->id,
                '_START_DATE_' => $ticket->event->start_date,
                '_END_DATE_' => $ticket->event->end_date,
                '_FEE_' => $ticket->amount, 
            ];
        } catch (Exception $e) {
            return [];
        }
        return [];
    }

    public function generateQrCode($file, $text)
    {
        $qrCode = (new QrCode($text))
            ->setSize(250)
            ->setMargin(5)
            ->useForegroundColor(0, 0, 0);

        $filename = \Yii::getAlias('@app').'/web/qr/qr_'.$file.'.png';
        $qrCode->writeFile($filename);
        return $filename;
    }
}
