<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {

    }

    public function actionIndex()
    {

        echo "- create-permission 'actionModuleController' 'this is the description' \n- create-role 'roleName'\n- assign-user 'id' 'roleName' \n assign-role 'roleName' 'actionModuleController'\n\n";
        echo "- add-child ParentRole ChildRole \n";
        echo "- get-role-permissions role \n";
        echo "- add-crud-role role ModuleController 'Add a Crud to Role'\n";
        echo "- remove-crud-role role ModuleController 'Remove a Crud from Role'\n";
        
        echo "- revoke-role 'roleName' 'actionModuleController'\n\n";

    }

    public function actionCreatePermission($name,$description)
    {
        echo "Creating Permission\n";
        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $permission = $auth->createPermission($name);
        $permission->description = $description;
        $auth->add($permission);
        echo "Permission $name created successfully\n";
        return 1;
    }

    public function actionCreateRole($name)
    {
        echo "adding a role\n";
        $auth = Yii::$app->authManager;
        $role = $auth->createRole($name);
        $auth->add($role);
    }

    public function actionGetRolePermissions($name)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($name);
        if($role){
            $permissions = $auth->getPermissionsByRole($name);
            foreach ($permissions as $permission => $key){
                echo "- ".$permission."\n";
            }

        }else{
            echo "Invalid Role $name \n";
        }
    }

    public function actionAssignUser($id,$role)
    {
        // the following three lines were added:
        $auth = \Yii::$app->authManager;
        $role = $auth->getRole($role);
        $auth->assign($role, $id);
        echo "Assigned user $id to role";
        return 1;
    }

    public function actionAssignRole($role,$permission)
    {
        // the following three lines were added:
        echo "Assigning role to permission\n";
        $auth = \Yii::$app->authManager;
        $role = $auth->getRole($role);
        $permission = $auth->getPermission($permission);
        if($permission){
            $auth->addChild($role, $permission);
        }else{
            echo "Invalid Permission ($permission) provided";
        }

        return 1;

    }

    public function actionRevokeRole($role,$permission)
    {
        // the following three lines were added:
        echo "Removing permission from role \n";
        $auth = \Yii::$app->authManager;
        $role = $auth->getRole($role);
        $permission = $auth->getPermission($permission);
        if($auth->hasChild($role, $permission)){
            $auth->removeChild($role, $permission);
        }else{
            echo "Role Doesnot have permission ($permission) attached";
        }

        return 1;

    }

    public function actionAddChild($role,$child)
    {
        // the following three lines were added:
        echo "Assigning role to permission\n";
        $auth = \Yii::$app->authManager;
        $role = $auth->getRole($role);
        $child = $auth->getRole($child);
        if($child){
            if($auth->addChild($role, $child))
                echo "Role Assigned Successfully";
        }else{
            echo "Invalid Role ($child) provided\n";
        }

        return 1;

    }

    public function actionAddCrudRole($role,$module)
    {

        echo "Starting Crud Operation of $role \n";
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role);
        $crud = ['index','view','create','update','delete'];
        $module = ucwords($module);
        foreach ($crud as $item){
            $name = $item.$module;
            echo "Creating Permission $name for $module \n";
            $permission = $auth->getPermission($name);
            if(!$permission){
                $permission = $auth->createPermission($name);
                $permission->description = $item. " ".$module;
                if($auth->add($permission)) {
                    echo "Permission $name created successfully\n";
                }
            }
            if(!$auth->hasChild($role, $permission)){
                if($auth->addChild($role, $permission)){
                    echo "Permission $name added to role \n";
                }else{
                    echo "An Error occurred\n ";
                }
            }else{
                echo "Role Already assigned Permission\n";
            }

        }
    }


    public function actionRemoveCrudRole($role,$module)
    {

        echo "Starting Crud Operation of $role \n";
        $auth = Yii::$app->authManager;
        $role = $auth->getRole($role);
        $crud = ['index','view','create','update','delete'];
        $module = ucwords($module);
        foreach ($crud as $item){
            $name = $item.$module;
            echo "Revoking Permission $name for $module \n";
            $permission = $auth->getPermission($name);

            if($auth->hasChild($role, $permission)){
                if($auth->removeChild($role, $permission)){
                    echo "Permission $name remove from role \n";
                }else{
                    echo "An Error occurred\n ";
                }
            }else{
                echo "Role doesnot have permission";
            }

        }
    }

}