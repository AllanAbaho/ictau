$(document).ready(function() {
    $.noConflict();
    $('#example').DataTable( {
        "dom": 'Bfrtip',
        "buttons": [
            'excel', 'pdf', 'print'
        ],
        "responsive": false,
        "serverSide": false,
        "paging": false,
        // "order": [[ 3, "desc" ]]
        "info": false,
        "searching": false,
    } );
    $('div.dt-buttons').css('display', 'block')
    $('div.dataTables_info').css('display', 'none')
    $('div.dt-buttons').css('text-align', 'right')
    $('div.dt-buttons > button:nth-child(2)').css('background', 'green')
    $('div.dt-buttons > button:nth-child(2)').css('color', 'white')
    $('div.dt-buttons > button:nth-child(1)').css('background', 'red')
    $('div.dt-buttons > button:nth-child(1)').css('color', 'white')
    $('div.dt-buttons > button:nth-child(3)').css('background', 'blue')
    $('div.dt-buttons > button:nth-child(3)').css('color', 'white')
} );