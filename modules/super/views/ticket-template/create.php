<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TicketTemplate */

$this->title = 'Create Ticket Template';
$this->params['breadcrumbs'][] = ['label' => 'Ticket Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
