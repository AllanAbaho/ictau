<?php

namespace app\modules\super;

/**
 * super module definition class
 */
class Super extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\super\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->layout = '/dashboard.php';
        \Yii::$app->errorHandler->errorAction = 'super/default/error';

        // custom initialization code goes here
    }
}
