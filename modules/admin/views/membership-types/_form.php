<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipTypes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->isNewRecord ? \Yii::t('app', 'Create Membership Type') : \Yii::t('app', 'Update Membership Type')?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['index'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
      <?php $form = ActiveForm::begin([
	      'fieldConfig' => [
		      'template' => "{input} {error}",
	      ]
      ]); ?>
        <div class="row">
            <div class="col-sm-12">
            <div class="form-group bmd-form-group">
                    <label for="<?=Html::getInputId($model, 'name');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'name');?></label>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false); ?>
                    <span class="bmd-help"><?=Html::activeHint($model,'name');?></span>
                </div>
                <div class="form-group bmd-form-group">
                    <label for="<?=Html::getInputId($model, 'fee');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'fee');?></label>
                    <?= $form->field($model, 'fee')->textInput(['maxlength' => true])->label(false); ?>
                    <span class="bmd-help"><?=Html::activeHint($model,'fee');?></span>
                </div>
                <div class="form-group bmd-form-group">
                    <label for="<?=Html::getInputId($model, 'template');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'template');?></label>
                    <?= $form->field($model, 'template')->textarea(['rows' => 12])->label(false); ?>
                    <span class="bmd-help"><?=Html::activeHint($model,'template');?></span>
                </div>
            </div>
    </div>
    <div class="card-footer ml-auto mr-auto">
        <?= Html::submitButton('Save', ['class' => 'btn btn-danger']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
    </div>
</div>