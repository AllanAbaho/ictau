<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipTypes */

$this->title = 'Add Membership Type';
$this->params['breadcrumbs'][] = ['label' => 'Membership Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
