<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipTypes */

$this->title = 'Update Membership Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Membership Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
