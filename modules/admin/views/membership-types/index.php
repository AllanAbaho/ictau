<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MembershipTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Membership Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Membership Types
              <div class="pull-right">
                <?= Html::a(Html::tag('b', 'add', ['class' => 'material-icons']) , ['create'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Add Membership Type'
                    ],
                ]) ?>
              </div>
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?php Pjax::begin([
                  'enablePushState'=>true,
              ]); ?>
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel,
                  'columns' => [
                      'name',
                      [
                        'label'=>'Fee',
                        'value' => function($data){
                            return 'UGX '. $data->fee;
                        }
                      ],
                      'created_at',
                      'updated_at',
                      [
                          'header' =>\Yii::t('app','Actions'),
                          'class' => '\yii\grid\ActionColumn',
                          'contentOptions' => [
                              'class'=>'table-actions'
                          ],
                          'template' => '{update} {delete}',
                          'buttons'  => [
	                          'view' => function ($url, $model) {
		                          return Html::a(
			                          '<i class="fa fa-eye"></i>',
			                          \yii\helpers\Url::to(['/admin/membership-types/view', 'id' => $model->id]),
			                          [
				                          'rel'                      => "tooltip",
				                          'data-original-title'      => 'View this Membership Type',
				                          'data-placement'           => 'top',
				                          'style'                    => 'margin-right: 10px'
			                          ]
		                          );
	                          },
	                          'update' => function ($url, $model) {
		                          return Html::a(
			                          '<i class="fa fa-edit"></i>',
			                          \yii\helpers\Url::to(['/admin/membership-types/update', 'id' => $model->id]),
			                          [
				                          'rel'                      => "tooltip",
				                          'data-original-title'      => 'Edit this Membership Type',
				                          'data-placement'           => 'top',
				                          'style'                    => 'margin-right: 10px'
			                          ]
		                          );
	                          },
	                          'delete' => function ($url, $model) {
		                          return Html::a(
			                          '<i class="fa fa-trash-o"></i>',
			                          \yii\helpers\Url::to(['/admin/membership-types/delete', 'id' => $model->id]),
			                          [
				                          'rel'                      => "tooltip",
				                          'data-original-title'      => 'Delete this Membership Type?',
				                          'data-placement'           => 'top',
				                          'data-pjax'                => '0',
				                          'data-confirm'             => 'Are you sure you want to delete this item?',
				                          'data-method'              => 'post',
				                          'style'                    => 'margin-right: 10px'
			                          ]
		                          );
	                          },
                          ]
                      ],
                  ],
              ]); ?>
              <?php Pjax::end(); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>