<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = 'Reject Application';
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?= 'Reject Application' ?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['index'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "{input} {error}",
                    ]
                ]); ?>

                <?= $form->field($model, 'status')->hiddenInput(['value' => 'Rejected'])->label(false) ?>
                <div class="form-group bmd-form-group">
                    <label for="<?=Html::getInputId($model, 'reason');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'reason');?></label>
                    <?= $form->field($model, 'reason')->textarea(['rows' => 6])->label(false); ?>
                    <span class="bmd-help"><?=Html::activeHint($model,'reason');?></span>
                </div>
                <?= Html::submitButton('Reject Application', ['class' => 'btn btn-danger']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
