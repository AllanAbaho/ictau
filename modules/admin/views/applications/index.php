<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ApplicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Applications
              <div class="pull-right">
              </div>
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?php Pjax::begin([
                  'enablePushState'=>true,
              ]); ?>
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel,
                  'columns' => [
                    [
                        'label' => 'Member Name',
                        'attribute' => 'user_id',
                        'value' => function($data){
                            return $data->user->name;
                        },
                    ],
                    [
                        'attribute' =>'status',
                        'filter'    => [ "On Hold"=>"On Hold", "Pending Approval"=>"Pending Approval", "Pending Payment"=>"Pending Payment", "Review"=>"Review", "Complete"=>"Complete", "Rejected"=>"Rejected" ],
                        'label' => 'Status',
                        'value' => function($data){
                            $labels = ['On Hold'=>'primary','Paid'=>'primary','Pending Approval'=>'warning','Pending License'=>'warning','Pending Payment'=>'info','Review'=>'default','Complete'=>'success','Rejected'=>'danger',];
                            return '<span style="padding:5px" class="alert alert-'.$labels[$data->status].'">'.$data->status.'</span>';
                    },
                        'format'=>'html'
                    ],            // 'category',
                    'payment.amount',
                    'payment.status:text:Payment Status',                      
                    [
                          'header' =>\Yii::t('app','Actions'),
                          'class' => '\yii\grid\ActionColumn',
                          'contentOptions' => [
                              'class'=>'table-actions'
                          ],
                          'template' => '{view}',
                          'buttons'  => [
	                          'view' => function ($url, $model) {
		                          return Html::a(
			                          '<i class="fa fa-eye"></i>',
			                          \yii\helpers\Url::to(['/admin/applications/view', 'id' => $model->id]),
			                          [
				                          'rel'                      => "tooltip",
				                          'data-original-title'      => 'View this Stage',
				                          'data-placement'           => 'top',
				                          'style'                    => 'margin-right: 10px'
			                          ]
		                          );
	                          },
                          ]
                      ],
                  ],
              ]); ?>
              <?php Pjax::end(); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>

