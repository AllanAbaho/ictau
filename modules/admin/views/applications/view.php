<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">account_box</i>
                </div>
                <h4 class="card-title">
                    <?=$model->user->name;?>
                    <div class="pull-right">
                    <?php if($model->status == 'Pending Approval' || $model->status == 'On Hold'): ?>
                        <?= Html::a('Approve Application', ['approve-application', 'id' => $model->id], [
                            'class' => 'btn btn-success',
                            'data' => [
                                'confirm' => 'Are you sure you want to approve this application?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php if($model->status !== 'On Hold'): ?>
                        <?= Html::a('Put On Hold', ['hold-application', 'id' => $model->id], [
                            'class' => 'btn btn-primary',
                            'data' => [
                                'confirm' => 'Are you sure you want to put this application on hold?',
                                'method' => 'post',
                            ],
                        ]) ?>
                        <?php endif; ?>
                        <?= Html::a('Reject Application', ['reject-application', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to reject this application?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php endif; ?>
                    <?php if($model->status == 'Paid'): ?>
                        <?= Html::a('Approve Payment', ['approve-payment', 'id' => $model->id], [
                            'class' => 'btn btn-success',
                            'data' => [
                                'confirm' => 'Are you sure you want to approve payment for this application?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php endif; ?>
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id:text:Application Number ',
                                'membership.name:text:Membership Type',
                                [
                                    'label'=>'Membership Fee',
                                    'value' => function($model){
                                        if($model->membership && $model->membership->fee){
                                            return 'UGX '. $model->membership->fee;
                                        }
                                    }
                                ],
                                'status',
                                'user.email',
                                'user.primary_phone_number',
                                'user.alternative_email',
                                'user.organisation',
                                'user.organisation_size',
                                'user.physical_address',
                                'user.alternative_phone_number',
                                'user.country_of_residence',
                                'user.facebook_handle',
                                'user.twitter_handle', 
                                'user.physical_address',
                                'user.other_phone_number',
                                [
                                    'attribute' => 'attachment',
                                    'label'=>'CV / Profile Attachment',
                                    'value' => function($data){
                                        if($data->user->attachment){
                                            $attachment = \Yii::getAlias('@app')."/web/profiles/" . $data->user->attachment;
                                            if(file_exists($attachment)) {
                                                return Html::a('Download CV / Profile Attachment',['/download/profile','file'=>$data->user->attachment]);
                                            }else{
                                                return "No Attachment";
                                            }    
                                        }
                                    },
                                    'format'=>'html',
                                ],

                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?php if($model->status == 'Paid' && $model->payment->payment_type == 'Bank'): ?>
                            <?php
                            $myFile = \Yii::getAlias('@app')."/web/receipts/" .$model->payment->file;
                            if(file_exists($myFile)){
                                $extension = pathinfo($myFile, PATHINFO_EXTENSION);
                                if($extension == 'pdf'){
                                    echo Html::a('Download Receipt',['/download/receipt','file'=>$model->payment->file]);
                                }else{
                                    echo Html::img('@web/receipts/'.$model->payment->file);
                                }
                            }
                            ?>
                        <?php elseif($model->status == 'Paid' && $model->payment->payment_type !== 'Bank'): ?>
                            <?php echo 'Please check your Olycash Wallet'; ?>
                        <?php endif; ?>
                    </div>    
                </div>            
            </div>
        </div>
    </div>
</div>

