<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GuestPayments */

$this->title = 'Create Guest Payments';
$this->params['breadcrumbs'][] = ['label' => 'Guest Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
