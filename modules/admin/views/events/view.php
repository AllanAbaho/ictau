<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Events */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->name;?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'edit', ['class' => 'material-icons']) , ['update', 'id'=>$model->id], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Update Event'
                    ],
                ]) ?>
              </div>          
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'start_date', 'end_date', 'name', 'cost', 'start_time', 'end_time', 'venue','members_cost'
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <img src="<?= Yii::getAlias('@web');?>/events/<?= $model->file;?>" width="300px" class="img-responsive" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
            <h3>Members</h3>
                <div class="material-datatables">
                    <?= GridView::widget([
                        'id' => 'users',
                        'tableOptions' => [
                            'class' => 'table table-striped table-no-bordered table-hover',
                        ],
                        'options' => ['class' => 'table-responsive grid-view'],
                        'dataProvider' => $member_payments,
                        'columns' => [
                            'user.name:text:Name',
                        ],
                    ]); ?>
                </div>
            </div>
            <div class="col-md-7">
            <h3>Guests</h3>
                <div class="material-datatables">
                    <?= GridView::widget([
                        'id' => 'users',
                        'tableOptions' => [
                            'class' => 'table table-striped table-no-bordered table-hover',
                        ],
                        'options' => ['class' => 'table-responsive grid-view'],
                        'dataProvider' => $guest_payments,
                        'columns' => [
                            'name',
                            'email',
                            'phone_number',
                        ],
                    ]); ?>
                </div>            
            </div>
        </div>
        <div class="row">
        <div class="col-md-12">
            <h3>Member Feedback</h3>
            <div class="material-datatables">
                    <?= GridView::widget([
                        'id' => 'users',
                        'tableOptions' => [
                            'class' => 'table table-striped table-no-bordered table-hover',
                        ],
                        'options' => ['class' => 'table-responsive grid-view'],
                        'dataProvider' => $member_feedback,
                        'columns' => [
                            'member.name',
                            'comment'
                        ],
                    ]); ?>
                </div>
        </div>
    </div>

    </div>
</div>
    </div>
</div>
