<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->isNewRecord ? \Yii::t('app', 'Create Event') : \Yii::t('app', 'Update Event')?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['index'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <?php $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{input} {error}",
                    ]
                ]); ?>
                    <div class="row">
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'name');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'name');?></label>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'name');?></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'cost');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'cost');?></label>
                <?= $form->field($model, 'cost')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'cost');?></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'members_cost');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'members_cost');?></label>
                <?= $form->field($model, 'members_cost')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'members_cost');?></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'cpd_points');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'cpd_points');?></label>
                <?= $form->field($model, 'cpd_points')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'cpd_points');?></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'venue');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'venue');?></label>
                <?= $form->field($model, 'venue')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'venue');?></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'start_time');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'start_time');?></label>
                <?= $form->field($model, 'start_time')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'start_time');?></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'end_time');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'end_time');?></label>
                <?= $form->field($model, 'end_time')->textInput(['maxlength' => true])->label(false); ?>
                <span class="bmd-help"><?=Html::activeHint($model,'end_time');?></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" id="formdate_picker6">
                <label class="bmd-label-floating">Start Date</label>
                <div class='input-group date'>
                    <?php if(!$model->start_date):?>
                        <input type="text" class="form-control" id="startdate" name="Events[start_date]" />
                    <?php else: ?>
                        <input type="text" class="form-control" id="startdate" name="Events[start_date]" value="<?=$model->start_date?>" />
                    <?php endif; ?>
                    <span class="input-group-addon"><i class="material-icons">today</i></span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" id="formdate_picker6">
                <label class="bmd-label-floating">End Date</label>
                <div class='input-group date'>
                    <?php if(!$model->end_date):?>
                        <input type="text" class="form-control" id="enddate" name="Events[end_date]" />
                    <?php else: ?>
                        <input type="text" class="form-control" id="enddate" name="Events[end_date]" value="<?=$model->end_date?>" />
                    <?php endif; ?>
                <span class="input-group-addon"><i class="material-icons">today</i></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group bmd-form-group">
                <label for="<?=Html::getInputId($model, 'description');?>" class="bmd-label-floating"><?=Html::activeLabel($model,'description');?></label>
                <?= $form->field($model, 'description')->textarea(['rows' => 6])->label(false) ?>
                <span class="bmd-help"><?=Html::activeHint($model,'description');?></span>
            </div>
        </div>
        <div class="col-md-6">
            <label for="myfile">Select a file:</label>
            <input type="file" id="myfile" name="Events[banner]"><br>
            <img src="<?= Yii::getAlias('@web');?>/events/<?= $model->file;?>" width="300px" class="img-responsive" />
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-danger']) ?>
    </div>


                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

