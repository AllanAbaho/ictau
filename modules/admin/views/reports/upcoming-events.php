<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upcoming Events Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              <?= $this->title; ?>
              <div class="pull-right">
              </div>          
        </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                      'id' => 'example'
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'columns' => [
                    'name',
                    [
                        'label'=>'Guest Fee',
                        'value' => function($data){
                            return 'UGX '. $data->cost;
                        }
                    ],
                    [
                        'label'=>'Members Fee',
                        'value' => function($data){
                            return 'UGX '. $data->members_cost;
                        }
                    ],
                    'start_date',
                    'end_date',
                  ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>
