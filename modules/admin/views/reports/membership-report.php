<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\MembershipTypes;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ApplicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Membership Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
            <?= $this->title; ?>
            <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

                <div class="pull-right">
                    <?= Html::a(Html::tag('b', 'add', ['class' => 'material-icons']) , ['/admin/users/create'], [
                        'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                        'rel'=>"tooltip",
                        'data' => [
                            'placement' => 'bottom',
                            'original-title' => 'Add New Member'
                        ],
                    ]) ?>
                </div>  
            </h4>

            <?php $form = ActiveForm::begin([
            'action' => ['membership-report'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group" id="formdate_picker6">
                    <label class="bmd-label-floating">Registered Date(From)</label>
                    <div class='input-group date'>
                        <input type="text" class="form-control" id="startdate" name="from_date" value="<?=$from_date?>" />
                        <span class="input-group-addon"><i class="material-icons">today</i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group" id="formdate_picker6">
                    <label class="bmd-label-floating">Registered Date(To)</label>
                    <div class='input-group date'>
                        <input type="text" class="form-control" id="enddate" name="to_date" value="<?=$to_date?>"/>
                        <span class="input-group-addon"><i class="material-icons">today</i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <label for="Membership Type">Membership Type</label><br>
                <?= Html::dropDownList('type_id', $type_id,
                    ArrayHelper::map(MembershipTypes::find()->all(), 'id', 'name'),['prompt'=>'Select Membership Type','class'=>'browser-default custom-select']); 
                ?>
            </div>
        </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                      'id' => 'example'
                  ],
                  'options' => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'columns' => [
                    [
                        'label' => 'Name',
                        'value' => function($data){
                            return $data->name;
                        }
                    ],
                    'email:email',
                    
                    [
                        'label' => 'Membership Type',
                        // 'attribute' => 'application_id',
                        'value' => function($data){
                            if($data->license && $data->license->application && $data->license->application->membership){
                                return $data->license->application->membership->name;
                            }
                        },
                    ],
                    'license.end_date:text:Expiry Date',
                    'created_at',
                    [
                        'label' => 'Status',
                        'value' => function($data){
                            if($data->license){
                                $today = date('Y-m-d');
                                if($data->license->end_date > $today){
                                    return '<span class="label label-success">Active</span>';
                                }else{
                                    return '<span class="label label-danger">Expired</span>';
                                }
                            }
                        },
                        'format'=>'html'
                    ],
                    [
                        'header' =>\Yii::t('app',''),
                        'class' => '\yii\grid\ActionColumn',
                        'contentOptions' => [
                            'class'=>'table-actions'
                        ],
                        'template' => '{view} {update} {delete}',
                        'buttons'  => [
                            'view' => function ($url, $model) {
                                if($model->license && $model->license->file){
                                    $file = \Yii::getAlias('@app')."/web/licenses/" . $model->license->file;
                                    if(file_exists($file)) {
    
                                        return Html::a(
                                            '<i class="fa fa-download"></i>',
                                            \yii\helpers\Url::to(['/download/index','file'=>$model->license->file]),
                                            [
                                                'rel'                      => "tooltip",
                                                'data-original-title'      => 'Download this certificate',
                                                'data-placement'           => 'top',
                                                'style'                    => 'margin-right: 10px',
                                                'data' => [
                                                    'method' => 'post',
                                                    'confirm' => 'Are you sure you want to download this certificate?',
                                                ],                                
                                            ]
                                        );
                                    }
                                }
                            },    
                          'update' => function ($url, $model) {
                            if($model->role !== 'Admin'){
                              return Html::a(
                                  '<i class="fa fa-edit"></i>',
                                  \yii\helpers\Url::to(['/admin/users/update', 'id' => $model->id]),
                                  [
                                      'rel'                      => "tooltip",
                                      'data-original-title'      => 'update this member',
                                      'data-placement'           => 'top',
                                      'style'                    => 'margin-right: 10px'
                                  ]
                              );
                            }
                          },
                          'delete' => function ($url, $model) {
                            return Html::a(
                                '<i class="fa fa-trash"></i>',
                                \yii\helpers\Url::to(['/admin/users/delete', 'id' => $model->id]),
                                [
                                    'rel'                      => "tooltip",
                                    'data-original-title'      => 'Delete this user',
                                    'data-placement'           => 'top',
                                    'style'                    => 'margin-right: 10px',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => 'Are you sure you want to delete this user?',
                                    ],                                
                                ]
                            );
                        },
                    ]
                  ],
                  ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>

