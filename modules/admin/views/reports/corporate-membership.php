<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ApplicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Corporate Membership Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              <?= $this->title; ?>
              <div class="pull-right">
              </div>
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                      'id' => 'example'
                  ],
                  'options' => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'columns' => [
                    'organisation',
                    'email:email',
                    [
                        'label' => 'Membership Type',
                        // 'attribute' => 'application_id',
                        'value' => function($data){
                            if($data->license && $data->license->application && $data->license->application->membership){
                                return $data->license->application->membership->name;
                            }
                        },
                    ],
                    'license.end_date:text:Expiry Date',
                    [
                        'label' => 'Status',
                        'value' => function($data){
                            if($data->license){
                                $today = date('Y-m-d');
                                if($data->license->end_date > $today){
                                    return '<span class="label label-success">Active</span>';
                                }else{
                                    return '<span class="label label-danger">Expired</span>';
                                }
                            }
                        },
                        'format'=>'html'
                    ],
                  ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>

