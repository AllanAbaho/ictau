<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->name;?>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'first_name',
                        'last_name',
                        'email:email',
                        'alternative_email:email',
                        'occupation',
                        'organisation',
                        'organisation_size',
                        'primary_phone_number',
                        'other_phone_number',
                        'physical_address',
                        'brief_bio:ntext',
                        'city_of_residence',
                        'county_of_residence',
                        'facebook_handle',
                        'twitter_handle',
                        'about_ictau:ntext',
                        'reference',
                        [
                            'attribute' => 'attachment',
                            'value' => function($data){
                                if($data->attachment){
                                    $attachment = \Yii::getAlias('@app')."/web/profiles/" . $data->attachment;
                                    if(file_exists($attachment)) {
                                        return Html::a('Download Profile Attacment',['/download/profile','file'=>$data->attachment]);
                                    }else{
                                        return "No Attachment";
                                    }    
                                }
                            },
                            'format'=>'html',
                        ],

                    ],
                ]) ?>
            </div>
            <div class="col-md-4">
                <div class="card-body-inner">
                    <img src="<?= Yii::getAlias('@web');?>/photos/<?= $model->photo;?>" class="img-responsive" />
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
