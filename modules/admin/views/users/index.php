<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Members
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'add', ['class' => 'material-icons']) , ['create'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Add New Member'
                    ],
                ]) ?>
            </div>  
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel, 
                  'columns' => [
                    'first_name',
                    'last_name',
                    'organisation',
                    'created_at',
                    [
                        'label' => 'Membership Type',
                        // 'attribute' => 'application_id',
                        'value' => function($data){
                            if($data->license && $data->license->application && $data->license->application->membership){
                                return $data->license->application->membership->name;
                            }
                        },
                    ],
                    'license.end_date:text:Expiry Date',
                    [
                        'attribute' => 'file',
                        'value' => function($data){
                            if($data->license && $data->license->file){
                                $file = \Yii::getAlias('@app')."/web/licenses/" . $data->license->file;
                                if(file_exists($file)) {
                                    return Html::a('Download Certificate',['/download/index','file'=>$data->license->file]);
                                }else{
                                    return "No Attachment";
                                }    
                            }
                        },
                        'format'=>'html',
                    ],

                    [
                        'label' => 'Status',
                        'value' => function($data){
                            if($data->license){
                                $today = date('Y-m-d');
                                if($data->license->end_date > $today){
                                    return '<span class="label label-success">Active</span>';
                                }else{
                                    return '<span class="label label-danger">Expired</span>';
                                }
                            }
                        },
                        'format'=>'html'
                    ],
                    [
                          'header' =>\Yii::t('app','Actions'),
                          'class' => '\yii\grid\ActionColumn',
                          'contentOptions' => [
                              'class'=>'table-actions'
                          ],
                          'template' => '{view} {update}',
                          'buttons'  => [
                            'view' => function ($url, $model) {
                                return Html::a(
                                    '<i class="fa fa-eye"></i>',
                                    \yii\helpers\Url::to(['/admin/users/view', 'id' => $model->id]),
                                    [
                                        'rel'                      => "tooltip",
                                        'data-original-title'      => 'View this Stage',
                                        'data-placement'           => 'top',
                                        'style'                    => 'margin-right: 10px'
                                    ]
                                );
                            },
                            'update' => function ($url, $model) {
                                return Html::a(
                                    '<i class="fa fa-edit"></i>',
                                    \yii\helpers\Url::to(['/admin/users/update', 'id' => $model->id]),
                                    [
                                        'rel'                      => "tooltip",
                                        'data-original-title'      => 'update this Stage',
                                        'data-placement'           => 'top',
                                        'style'                    => 'margin-right: 10px'
                                    ]
                                );
                            },
                      ]
                    ],
                ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>


