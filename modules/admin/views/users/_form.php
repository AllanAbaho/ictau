<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $license app\models\Users */
/* @var $form yii\widgets\ActiveForm */
$types = ArrayHelper::map(\app\models\MembershipTypes::find()->all(),'id','name');

?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?= 'User Management' ?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['/admin/reports/membership-report'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-8">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "{input} {error}",
                    ]
                ]); ?>
<?= $form->field($model, 'role')
            ->dropDownList(['Member'=>'Member','Organisation'=>'Organisation'], ['prompt'=>'Select Account Type','class'=>'browser-default custom-select','required'=>true]) ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true,'placeholder'=>'First Name']) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true,'placeholder'=>'Last Name']) ?>

<?= $form->field($model, 'organisation')->textInput(['maxlength' => true,'placeholder'=>'Organisation']) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'Email Address']) ?>

<?= $form->field($model, 'primary_phone_number')->textInput(['maxlength' => true,'placeholder'=>'Phone Number']) ?>
<?php if($application): ?>
<?= $form->field($application, 'membership_type_id')
            ->dropDownList($types, ['prompt'=>'Select Membership Type','class'=>'browser-default custom-select']) ?>
<?php endif; ?>
<?php if($license): ?>
<div class="form-group" id="formdate_picker6">
    <div class='input-group date'>
        <?php if($license && $license->end_date):?>
            <input value="<?=$license->end_date ?>" type="text" class="form-control" id="startdate" name="Licenses[end_date]" placeholder="Membership Expiry Date"/>
        <?php else: ?>
            <input type="text" class="form-control" id="startdate" name="Licenses[end_date]" placeholder="Membership Expiry Date"/>
        <?php endif; ?>
        <span class="input-group-addon"><i class="material-icons">today</i></span>
    </div>
</div>
<?php endif; ?>

<div class="form-group">
<?= Html::submitButton('Save', ['class' => 'btn btn-danger']) ?>
</div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
