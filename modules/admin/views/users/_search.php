<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'alternative_email') ?>

    <?php // echo $form->field($model, 'occupation') ?>

    <?=  $form->field($model, 'organisation') ?>

    <?php // echo $form->field($model, 'organisation_size') ?>

    <?php // echo $form->field($model, 'primary_phone_number') ?>

    <?php // echo $form->field($model, 'alternative_phone_number') ?>

    <?php // echo $form->field($model, 'other_phone_number') ?>

    <?php // echo $form->field($model, 'physical_address') ?>

    <?php // echo $form->field($model, 'brief_bio') ?>

    <?php // echo $form->field($model, 'membership_type_id') ?>

    <?php // echo $form->field($model, 'city_of_residence') ?>

    <?php // echo $form->field($model, 'county_of_residence') ?>

    <?php // echo $form->field($model, 'facebook_handle') ?>

    <?php // echo $form->field($model, 'twitter_handle') ?>

    <?php // echo $form->field($model, 'about_ictau') ?>

    <?php // echo $form->field($model, 'other_comments') ?>

    <?php // echo $form->field($model, 'reference') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'auth_key') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'is_notified') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
