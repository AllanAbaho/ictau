<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EventPaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-payments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Event Payments', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'payment_type',
            'event_id',
            'amount',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            //'name',
            //'phone_number',
            //'reference_number',
            //'file',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
