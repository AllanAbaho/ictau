<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventPayments */

$this->title = 'Update Event Payments: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Event Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-payments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
