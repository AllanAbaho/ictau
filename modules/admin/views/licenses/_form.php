<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Licenses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licenses-form">
<div class="card-body-inner">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-fellowships-date required">
        <label class="control-label" for="fellowships-date"> Start Date</label>
        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
        <?php if($model->start_date): ?>
            <input value="<?=$model->start_date ?>" type="text" id="fellowships-date" class="form-control" name="Licenses[start_date]" aria-required="true" aria-invalid="false">
        <?php else: ?>
        <input type="text" id="fellowships-date" class="form-control" name="Licenses[start_date]" aria-required="true" aria-invalid="false">
        <?php endif; ?>
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
        <div class="help-block"></div>
    </div>
    <div class="form-group field-fellowships-date required">
        <label class="control-label" for="fellowships-date"> End Date</label>
        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
        <?php if($model->end_date): ?>
            <input value="<?=$model->end_date ?>" type="text" id="fellowships-date" class="form-control" name="Licenses[end_date]" aria-required="true" aria-invalid="false">
        <?php else: ?>
        <input type="text" id="fellowships-date" class="form-control" name="Licenses[end_date]" aria-required="true" aria-invalid="false">
        <?php endif; ?>
            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        </div>
        <div class="help-block"></div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
