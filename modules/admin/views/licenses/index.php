<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\LicensesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licenses-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="card-body-inner">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Member Name',
                'attribute' => 'user_id',
                'value' => function($data){
                    return $data->user->name;
                },
            ],
            [
                'label' => 'Membership Type',
                'attribute' => 'application_id',
                'value' => function($data){
                    if($data->application && $data->application->membership){
                        return $data->application->membership->name;
                    }
                },
            ],
            'start_date',
            'end_date',
            //'updated_at',
            //'created_at',
            //'updated_by',
            //'created_by',

            // [
            //     'attribute'=>'',
            //     'value' => function($data){
            //         return Html::a('View',['view','id'=>$data->id]);
            //     },
            //     'format'=>'html'
            // ],
        ],
    ]); ?>
    </div>

</div>
