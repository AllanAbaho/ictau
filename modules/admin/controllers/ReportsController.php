<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Users;
use app\models\Licenses;
use app\models\Events;
use app\models\Payments;
use app\models\EventPayments;
use app\models\EventFeedback;
use app\models\GuestPayments;
use app\models\Applications;
use app\models\Notifications;
use app\modules\admin\models\UsersSearch;

class ReportsController extends Controller
{
    // public $layout = '/dashboard/main.php';
    /**
     * {@inheritdoc}
     */
    

    /**
     * Displays homepage.
     *
     * @return string
     */
    // public function actionIndex()
    // {
    //     $model = new LoginForm();

    //     return $this->render('index', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionIndividualMembership()
    {
        $query = Users::find()->where(['role'=>'Member'])
        ->joinWith('licences.application.memberhip')
        ->andWhere(['like','membership_types.name','Individual']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('individual-membership', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCorporateMembership()
    {
        $query = Users::find()->where(['role'=>'Organisation'])
        ->joinWith('licences.application.memberhip')
        ->andWhere(['like','membership_types.name','%corporate%',false])
        ->orWhere(['like','membership_types.name','%government%',false]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('corporate-membership', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStudentMembership()
    {
        $query = Users::find()->where(['role'=>'Member'])
        ->joinWith('licences.application.memberhip')
        ->andWhere(['like','membership_types.name','Student']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('student-membership', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegisteredMembers()
    {
        $query = Users::find()->where(['role'=>'Member'])
        ->joinWith('licences.application.memberhip');
        // ->andWhere(['like','membership_types.name','Corporate']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('registered-members', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActiveMembers()
    {
        $query = Users::find()->where(['role'=>'Member'])
        ->joinWith('licences')
        ->andWhere(['>=','licenses.end_date',date('Y-m-d')]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('active-members', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMembershipReports($from_date=null, $to_date=null,$type_id=null)
    {
        $from_date = $from_date ? $from_date : date('2021-01-01');
        $to_date = $to_date ? $to_date : date('Y-12-31');

        if(!$type_id){
            $query = Users::find();
        }else{
            $query = Users::find()
            ->joinWith('licences.application.memberhip')
            ->andWhere(['>=','licenses.created_at',$from_date])
            ->andWhere(['<=','licenses.created_at',$to_date])
            ->andWhere(['membership_types.id'=>$type_id]);    
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);
        // var_dump($dataProvider);
        // die();

        return $this->render('membership-report', [
            'dataProvider' => $dataProvider,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'type_id' => $type_id,
        ]);
    }

        /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionMembershipReport()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCorporateReport()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->csearch(Yii::$app->request->queryParams);

        return $this->render('indexc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionInactiveMembers()
    {
        $query = Users::find()->where(['role'=>'Member'])
        ->joinWith('licences')
        ->andWhere(['<','licenses.end_date',date('Y-m-d')]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('inactive-members', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpcomingEvents()
    {
        $query = Events::find()->where(['>=','end_date',date('Y-m-d')]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('upcoming-events', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPastEvents()
    {
        $query = Events::find()->where(['<','end_date',date('Y-m-d')]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('past-events', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMembershipPayments()
    {
        $query = Payments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('membership-payments', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEventPaymentsMembers()
    {
        $query = EventPayments::find()->where(['>','amount',0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('event-payments-members', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEventPaymentsGuests()
    {
        $query = GuestPayments::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $this->render('event-payments-guests', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDelete($id)
    {
        Licenses::deleteAll(['user_id'=>$id]);
        EventPayments::deleteAll(['user_id'=>$id]);
        Payments::deleteAll(['user_id'=>$id]);
        Applications::deleteAll(['user_id'=>$id]);
        Notifications::deleteAll(['user_id'=>$id]);
        EventFeedback::deleteAll(['user_id'=>$id]);
        Users::findOne($id)->delete();
        
        return $this->redirect(['/admin/reports/membership-report']);
    }


 
}
