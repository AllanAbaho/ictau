<?php

namespace app\modules\admin;

/**
 * Admin module definition class
 */
class admin extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->layout = '/dashboard/main.php';
        \Yii::$app->errorHandler->errorAction = 'admin/default/error';
    }
}
