<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = 'Make Payment';
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$this->title;?>
            <div class="pull-right">
            </div>
        </h4>
    </div>
    <div class="card-body">

    <?= $this->render('_form', [
        'model' => $model,
        'application' => $application,
        ]) ?>

</div>
