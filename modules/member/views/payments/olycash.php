<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$this->title = 'Oly Cash';
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['payments/olycash','id'=>$application->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">account_box</i>
            </div>
            <h4 class="card-title">
                Pay With OlyCash
            </h4>
          </div>
          <div class="card-body">
            <script>(function(d, s, id) {var js, ojs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "https://share.olycash.com/en-us/sdk.js";ojs.parentNode.insertBefore(js, ojs);}(document, 'script', 'olycash-js-sdk'));</script>
            <!-- OPTIONAL: You can remove and incorporate this table in your form if desired -->
            <table class="amount-entry-form" style="border-collapse: collapse !important; max-width:250px !important;"><tr>
                <td style="border:0px;font-weight:bold;font-family: Arial, sans-serif; font-size: 17px;">Amount: </td>
                <td style="border:0px;"><select id="olycash__tempcurrency_1543470912" name="olycash__tempcurrency_1543470912" class="__olycash-temp-currency-field" style="width:85px;height:40px;padding:9px 5px 8px 5px;border: 1px solid #B2B2B2; background-color: #FFFFFF;font-family: Arial, sans-serif; font-size: 17px;"><option value="" disabled>Currency</option><option value="UGX" selected>UGX</option><option value="KES">KES</option><option value="TZS">TZS</option><option value="GHS">GHS</option><option value="ARS">ARS</option><option value="AUD">AUD</option><option value="BRL">BRL</option><option value="CAD">CAD</option><option value="CHF">CHF</option><option value="CZK">CZK</option><option value="DKK">DKK</option><option value="EUR">EUR</option><option value="GBP">GBP</option><option value="HKD">HKD</option><option value="HUF">HUF</option><option value="ILS">ILS</option><option value="JPY">JPY</option><option value="MXN">MXN</option><option value="MYR">MYR</option><option value="NOK">NOK</option><option value="NZD">NZD</option><option value="PHP">PHP</option><option value="PLN">PLN</option><option value="RUB">RUB</option><option value="SEK">SEK</option><option value="SGD">SGD</option><option value="THB">THB</option><option value="TRY">TRY</option><option value="TWD">TWD</option><option value="USD">USD</option></select></td>
                <td style="border:0px;">
                <input type="text" id="olycash__temptotal_1543470912" name="olycash__temptotal_1543470912" style="width:100px;height:40px;padding:10px;border: 1px solid #B2B2B2; background-color: #FFFFFF;font-family: Arial, sans-serif; font-size: 17px;" class="__olycash-temp-amount-field" value="<?=$application->membership->fee;?>" readonly="readonly" placeholder="0.00" maxlength="12"></td></tr>
            </table><!-- END OPTIONAL --><!-- Your share code -->
                
            <!-- For Dev Testing Please uncomment the line below -->
            <!-- <div class="olycash-pay debit-only olycash--window" style="width:100% !important;" data-id="133C9BD_8B35E"> -->
            <div class="olycash-pay olycash--window" style="width:100% !important;" data-ignorefrequency="Y" data-id="1344F48_BC3461"> 
                <input type="hidden" id="olycash__category" name="olycash__category" value="156"/>
                <input type="hidden" id="olycash__total" name="olycash__total" value="<?=$application->membership->fee;?>"/>
                <input type="hidden" id="olycash__currency" name="olycash__currency" value="UGX"/>
                <input type='hidden' id='olycash__post_process' name='olycash__post_process' value='olycashPostProcess'/>
                <input type='hidden' id='olycash__post_response' name='olycash__post_response' value='<?= \yii\helpers\Url::to(['payments/confirm','id'=>$application->id],true);?>'/>
                <input type='hidden' id='olycash__buyer_name' name='hidden' value="<?= $application->user->name ?>"/>
                <input type='hidden' id='olycash__third_party_fee_paid_by' name='olycash__third_party_fee_paid_by' value="payer"/>        
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    (function() {
        var callbackUrl = '<?= \yii\helpers\Url::to(['confirm','id'=>$application->id],true); ?>';
    console.log(callbackUrl)

})();



    function olycashPostProcess(data){
        console.log(data);
        notifyPaymentCompletion(data);
    }

    function notifyPaymentCompletion(data) {
        // var callbackUrl = \yii\helpers\Url::to(['confirm','id'=>$application->id],true);
        jQuery.post('<?= \yii\helpers\Url::to(['confirm','id'=>$application->id],true);?>', {'purchase_id': data.purchase_id, 'message': data.message, 'payment_type': data.payment_type}, function(response){
            console.log('Achest API notified. Response from API: '+JSON.stringify(response));
        });
    }

</script>
