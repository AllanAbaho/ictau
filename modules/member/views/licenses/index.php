<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Certificates
              <div class="pull-right">
              </div>
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'columns' => [
                    'user.name',
                    'start_date',
                    'end_date',
                    [
                        'attribute' => 'file',
                        'value' => function($data){
                            if($data->file){
                                $file = \Yii::getAlias('@app')."/web/licenses/" . $data->file;
                                if(file_exists($file)) {
                                    return Html::a('Download Certificate',['/download/index','file'=>$data->file]);
                                }else{
                                    return "No Attachment";
                                }    
                            }
                        },
                        'format'=>'html',
                    ],
                ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>

