<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user app\users\Users */
/* @var $form yii\widgets\ActiveForm */
$occupations = \yii\helpers\ArrayHelper::map(\app\models\Occupations::find()->all(), 'id', 'name');
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?= 'Update User' ?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['index'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
            <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'alternative_email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'organisation_size')
            ->dropDownList(['Up to 5' => 'Up to 5','5 to 10' => '5 to 10','10 to 20' => '10 to 20','More than 20' => 'More than 20']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'other_phone_number')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'physical_address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'facebook_handle')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'twitter_handle')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($user, 'about_ictau')
            ->dropDownList(['Facebook' => 'Facebook','Twitter' => 'Twitter','Our Website' => 'Linked In','Student' => 'Student']) ?>
        </div>
        <div class="col-md-3">
            <label for="receipt">Upload CV / Company Profile:</label>
            <input type="file" name="Users[attachment_image]"><br>
            <?= $user->attachment ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'brief_bio')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'reference')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'website')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <label for="receipt">Upload Profile Photo:</label>
            <input type="file" name="Users[photo_image]"><br>
            <img src="<?= Yii::getAlias('@web');?>/photos/<?= $user->photo;?>" width="250px" class="img-responsive" />
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>