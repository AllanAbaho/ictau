<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $user app\models\Applications */

$this->title = 'Dashboard';
$notifications = \app\models\Notifications::find()->where(['user_id'=>\Yii::$app->user->id])->orderBy('id desc')->limit(3)->all();

?>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">content_copy</i>
            </div>
            <p class="card-category">Current Membership</p>
            <?php if($license): ?>
            <h5 class="card-title"><?= $license->application->membership->name; ?></h5>
            <?php endif; ?>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">library_books</i>
              <a href="<?= Url::to(['/member/applications/change']);?>">Change Membership</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
              <i class="material-icons">store</i>
            </div>
            <p class="card-category">Expiration Date</p>
            <?php if($license): ?>
            <h5 class="card-title"><?= $license->end_date ?></h5>
            <?php endif; ?>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">date_range</i> 
              <?php if($license && $license->end_date < date('Y-m-d', strtotime('+2 months')) ): ?>
              <a href="<?= Url::to(['/member/applications/renew-membership']);?>">Renew Membership</a>
              <?php else: ?>
                Membership still active
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">info_outline</i>
            </div>
            <p class="card-category">Membership Payments</p>
            <h5 class="card-title"><?= $paymentsTotal . ' UGX' ?></h5>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">local_offer</i> 
              Total payments made
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
          <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
              <i class="fa fa-twitter"></i>
            </div>
            <p class="card-category">Events Attended</p>
            <h5 class="card-title"><?= $eventsCount ?></h5>
          </div>
          <div class="card-footer">
            <div class="stats">
              <i class="material-icons">update</i> 
              Total events attended
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">account_box</i>
            </div>
            <h4 class="card-title">
                Recent Applications
            </h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
                <?php Pjax::begin([
                    'enablePushState'=>true,
                ]); ?>
                <?= GridView::widget([
                    'id' => 'users',
                    'tableOptions' => [
                        'class' => 'table table-striped table-no-bordered table-hover',
                    ],
                    'options' => ['class' => 'table-responsive grid-view'],
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'label' => 'Member Name',
                            'attribute' => 'user_id',
                            'value' => function($data){
                                return $data->user->name;
                            },
                        ],
                        [
                            'attribute' =>'status',
                            'filter'    => [ "On Hold"=>"On Hold", "Pending Approval"=>"Pending Approval", "Review"=>"Review", "Complete"=>"Complete", "Rejected"=>"Rejected" ],
                            'label' => 'Status',
                            'value' => function($data){
                              $labels = ['On Hold'=>'primary','Paid'=>'primary','Pending Approval'=>'warning','Pending License'=>'warning','Pending Payment'=>'info','Review'=>'default','Complete'=>'success','Rejected'=>'danger',];
                              return '<span style="padding:5px" class="alert alert-'.$labels[$data->status].'">'.$data->status.'</span>';
                            },
                            'format'=>'html'
                        ], 
                      'created_at',
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
      <div class="card">
          <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
              <i class="material-icons">notifications</i>
            </div>
            <h4 class="card-title">
                Notifications
            </h4>
          </div>
          <div class="card-body">
            <?php foreach($notifications as $notification): ?>
              <b><?= $notification->subject ?></b><br>
              <?= $notification->message ?><br>
              <?= $notification->created_at ?><br><br>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
