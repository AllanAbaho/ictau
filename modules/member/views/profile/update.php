<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
    <?php if($user->role == 'Member'): ?>
        <?= $this->render('_form', [
            'user' => $user,
        ]) ?>
    <?php elseif($user->role == 'Organisation'): ?>
        <?= $this->render('_form_organisation', [
            'user' => $user,
        ]) ?>
    <?php endif; ?>    

