<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $user app\models\Applications */

$this->title = $user->name;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content">
    <div class="container-fluid">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$user->name;?>
            <div class="pull-right">
                <?= Html::a('Update Profile', ['update', 'id' => $user->id], [
                    'class' => 'btn btn-danger',
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <div class="row">
            <?php if($user->role == 'Member'): ?>
                <div class="col-md-8">
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'first_name',
                            'last_name',
                            'email:email',
                            'alternative_email:email',
                            [
                                'label' => 'Occupation',
                                'value' => function($model){
                                    if($model->job){
                                        return $model->job->name;
                                    }
                                }
                            ],
                            'organisation',
                            'organisation_size',
                            'primary_phone_number',
                            'other_phone_number',
                            'physical_address',
                            'brief_bio:ntext',
                            'city_of_residence',
                            'county_of_residence',
                            'facebook_handle',
                            'twitter_handle',
                            'about_ictau:ntext',
                            'reference',
                            [
                                'attribute' => 'attachment',
                                'label'=>'CV Attachment',
                                'value' => function($data){
                                    if($data->attachment){
                                        $attachment = \Yii::getAlias('@app')."/web/profiles/" . $data->attachment;
                                        if(file_exists($attachment)) {
                                            return Html::a('Download CV Attachment',['/download/profile','file'=>$data->attachment]);
                                        }else{
                                            return "No Attachment";
                                        }    
                                    }
                                },
                                'format'=>'html',
                            ],

                        ],
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <div class="card-body-inner">
                        <img src="<?= Yii::getAlias('@web');?>/photos/<?= $user->photo;?>" width="300px" class="img-responsive" />
                    </div>
                </div>
                <?php elseif($user->role == 'Organisation'): ?>
                    <div class="col-md-8">
                    <?= DetailView::widget([
                        'model' => $user,
                        'attributes' => [
                            'email:email',
                            'alternative_email:email',
                            'organisation',
                            'organisation_size',
                            'primary_phone_number',
                            'other_phone_number',
                            'physical_address',
                            'brief_bio:ntext',
                            'facebook_handle',
                            'twitter_handle',
                            'website',
                            'about_ictau:ntext',
                            'reference',
                            [
                                'attribute' => 'attachment',
                                'label'=>'Profile Attachment',
                                'value' => function($data){
                                    if($data->attachment){
                                        $attachment = \Yii::getAlias('@app')."/web/profiles/" . $data->attachment;
                                        if(file_exists($attachment)) {
                                            return Html::a('Download Profile Attachment',['/download/profile','file'=>$data->attachment]);
                                        }else{
                                            return "No Attachment";
                                        }    
                                    }
                                },
                                'format'=>'html',
                            ],

                        ],
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <div class="card-body-inner">
                        <img src="<?= Yii::getAlias('@web');?>/photos/<?= $user->photo;?>" width="300px" class="img-responsive" />
                    </div>
                </div>
                <?php endif; ?>
        </div>
    </div>
</div>
    </div>
</div>
