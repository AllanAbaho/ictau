<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upcoming Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <div class="container-fluid">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Events
              <div class="pull-right">
              </div>          
        </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                //   'filterModel' => $searchModel,
                  'columns' => [
                    'name',
                    // [
                    //     'label'=>'Guest Fee',
                    //     'value' => function($data){
                    //         return 'UGX '. $data->cost;
                    //     }
                    // ],
                    [
                        'label'=>'Members Fee',
                        'value' => function($data){
                            return 'UGX '. $data->members_cost;
                        }
                    ],
                    // 'cpd_points',
                    'payment.status',
                    [
                        'label' => 'Ticket',
                        'value' => function($data){
                            if($data->payment && $data->payment->file){
                                $file = \Yii::getAlias('@app')."/web/tickets/" . $data->payment->file;
                                if(file_exists($file)) {
                                    return Html::a('Download Ticket',['/download/ticket','file'=>$data->payment->file],['target'=>'_blank']);
                                }else{
                                    return "No Ticket";
                                }    
                            }
                        },
                        'format'=>'html',
                    ],
                    [
                        'label' => 'Receipt',
                        'value' => function($data){
                            if($data->payment && $data->payment->receipt){
                                $receipt = \Yii::getAlias('@app')."/web/receipts/" . $data->payment->receipt;
                                if(file_exists($receipt)) {
                                    return Html::a('Download Receipt',['/download/receipt','file'=>$data->payment->receipt]);
                                }else{
                                    return "No Receipt";
                                }    
                            }
                        },
                        'format'=>'html',
                    ],
                                [
                          'header' =>\Yii::t('app','Actions'),
                          'class' => '\yii\grid\ActionColumn',
                          'contentOptions' => [
                              'class'=>'table-actions'
                          ],
                          'template' => '{view}',
                          'buttons'  => [
                            'view' => function ($url, $model) {
                                return Html::a(
                                    '<i class="fa fa-eye"></i>',
                                    \yii\helpers\Url::to(['/member/events/view', 'id' => $model->id]),
                                    [
                                        'rel'                      => "tooltip",
                                        'data-original-title'      => 'View this Stage',
                                        'data-placement'           => 'top',
                                        'style'                    => 'margin-right: 10px'
                                    ]
                                );
                            },
                      ]
                      ],
                  ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>
