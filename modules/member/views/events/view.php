<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">account_box</i>
                </div>
                <h4 class="card-title">
                    <?=$model->name;?>
                    <div class="pull-right">
                        <?php if(!$alreadyPaid && $model->end_date >= date('Y-m-d')): ?>
                            <?php if($model->members_cost > 0): ?>
                                <?= Html::a('Make Payment', ['event-payments/olycash', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to make payment for this event?',
                                    ],
                                ]) ?>
                            <?php else: ?>
                                <?= Html::a('Confirm Attendance', ['event-payments/confirm-attendance', 'id' => $model->id], [
                                    'class' => 'btn btn-primary',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to confirm attendance for this event?',
                                    ],
                                ]) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'start_date', 'end_date', 'name', 'cost', 'start_time', 'end_time', 'venue','members_cost'
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= Yii::getAlias('@web');?>/events/<?= $model->file;?>" width="300px" class="img-responsive" />
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <?php if($payment): ?>
                            <h4>Payment Information</h4>
                            <?= DetailView::widget([
                                'model' => $payment,
                                'attributes' => [
                                    'status',
                                    [
                                        'attribute' => 'file',
                                        'value' => function($data){
                                            if($data->file){
                                                $file = \Yii::getAlias('@app')."/web/tickets/" . $data->file;
                                                if(file_exists($file)) {
                                                    return Html::a('Download Ticket',['/download/ticket','file'=>$data->file]);
                                                }else{
                                                    return "No Attachment";
                                                }    
                                            }
                                        },
                                        'format'=>'html',
                                    ],
                        
                                ],
                            ]) ?>
                        <?php endif; ?>
                    </div>         

                    <div class="col-md-6">
                        <h4>Give Us your Feedback</h4>
                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($feedback, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false) ?>
                        <?= $form->field($feedback, 'event_id')->hiddenInput(['value' => $model->id])->label(false) ?>

                        <?= $form->field($feedback, 'comment')->textarea(['rows' => 4]) ?>

                        <div class="form-group">
                            <?php if(!$feedback->id): ?>
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            <?php else: ?>
                                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
