<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="content">
    <div class="container-fluid">
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

        <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">account_box</i>
          </div>
          <h4 class="card-title">
              Applications
              <div class="pull-right">
                <?= Html::a('Apply For Membership', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('Change Membership', ['change'], ['class' => 'btn btn-primary']) ?>
              </div>
          </h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
              <?= GridView::widget([
                  'id' => 'users',
                  'tableOptions' => [
                      'class' => 'table table-striped table-no-bordered table-hover',
                  ],
                  'options'          => ['class' => 'table-responsive grid-view'],
                  'dataProvider' => $dataProvider,
                  'columns' => [
                    [
                        'label' => 'Member Name',
                        'attribute' => 'user_id',
                        'value' => function($data){
                            return $data->user->name;
                        },
                    ],
                    [
                        'attribute' =>'status',
                        'filter'    => [ "On Hold"=>"On Hold", "Pending Approval"=>"Pending Approval", "Review"=>"Review", "Complete"=>"Complete", "Rejected"=>"Rejected" ],
                        'label' => 'Status',
                        'value' => function($data){
                            $labels = ['On Hold'=>'primary','Paid'=>'primary','Pending Approval'=>'warning','Pending License'=>'warning','Pending Payment'=>'info','Review'=>'default','Complete'=>'success','Rejected'=>'danger',];
                            return '<span style="padding:5px" class="alert alert-'.$labels[$data->status].'">'.$data->status.'</span>';
                    },
                        'format'=>'html'
                    ],            // 'category',
                    'payment.status:text:Payment Status',   
                    'payment.amount:text:Amount Paid',                   
                    [
                          'header' =>\Yii::t('app','Actions'),
                          'class' => '\yii\grid\ActionColumn',
                          'contentOptions' => [
                              'class'=>'table-actions'
                          ],
                          'template' => '{view}{bank}{online}', 
                          'buttons'  => [
	                          'view' => function ($url, $model) {
		                          return Html::a(
			                          '<i class="fa fa-eye"></i>',
			                          \yii\helpers\Url::to(['/member/applications/view', 'id' => $model->id]),
			                          [
				                          'rel'                      => "tooltip",
				                          'data-original-title'      => 'View this Stage',
				                          'data-placement'           => 'top',
				                          'style'                    => 'margin-right: 10px'
			                          ]
		                          );

                                  

                                  
	                          },

                              'bank' => function ($url, $model) {
                                

                                if($model->status == 'Pending Payment'){

                                  return Html::a(
                                    'Bank Payment',
                                    \yii\helpers\Url::to(['/member/payments/create', 'id' => $model->id]),
                                    [
                                        'rel'                      => "tooltip",
                                        'data-original-title'      => 'View this Stage',
                                        'data-placement'           => 'top',
                                        'style'                    => 'margin-right: 10px',
                                        'class' => 'btn btn-sm btn-success',
                                        'data' => [
                                          'confirm' => 'Are you sure you want to make payment for this application?',
                                      ],
                                    ]
                                  );

                              }

                                            

                                
                            },

                            'online' => function ($url, $model) {
                                

                               

                                              if ($model->status == 'Pending Payment') {
                                                  return Html::a(
                                                      'Online Payment',
                                                      \yii\helpers\Url::to(['/member/payments/olycash', 'id' => $model->id]),
                                                      [
                                                          'rel' => "tooltip",
                                                          'data-original-title' => 'View this Stage',
                                                          'data-placement' => 'top',
                                                          'style' => 'margin-right: 10px',
                                                          'class' => 'btn btn-sm btn-primary',
                                                          'data' => [
                                                              'confirm' => 'Are you sure you want to make payment for this application?',
                                                          ],
                                                      ]
                                                  );

                                              }

                                
                            },
                          ]
                      ],
                  ],
              ]); ?>
          </div>
        </div>
      </div>
    </div>
</div>
    </div>
</div>
