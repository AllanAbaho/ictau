<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->user->name;?>
            <div class="pull-right">
            <?php if($model->status == 'Review'): ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Submit Application', ['submit-application', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to submit this application?',
            ],
        ]) ?>
    <?php endif; ?>
    <?php if($model->status == 'Pending Payment'): ?>
        <?= Html::a('Bank Payment', ['payments/create', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to make payment for this application?',
            ],
        ]) ?>
        <?= Html::a('Online Payment', ['payments/olycash', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Are you sure you want to make payment for this application?',
            ],
        ]) ?>
    <?php endif; ?>

            </div>
        </h4>
    </div>
    <div class="card-body">
        <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
                'id:text:Application Number ',
                'membership.name:text:Membership Type',
                [
                    'label'=>'Membership Fee',
                    'value' => function($model){
                        return 'UGX '. $model->membership->fee;
                    }
                ],
                'status',
                [
                    'label'=>'Reason for Rejection',
                    'value' => function($model){
                        if($model->reason){
                            return $model->reason;
                        }else{
                            return 'N/A';
                        }
                    }
                ],
        ],
        ]) ?>
    </div>
</div>
    </div>
</div>
