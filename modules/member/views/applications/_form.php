<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
/* @var $form yii\widgets\ActiveForm */
$types = \app\models\MembershipTypes::find()
    ->where(['like','name','%student%',false])
    ->orWhere(['like','name','%individual%',false])
    ->all();
$orgtypes = \app\models\MembershipTypes::find()
    ->where(['like','name','%corporate%',false])
    ->orWhere(['like','name','%government%',false])
    ->all();
if($user->role == 'Organisation'){
    $types = $orgtypes;
}
?>
<div class="content">
    <div class="container-fluid">
        <div class="card ">
    <div class="card-header card-header-danger card-header-icon">
        <div class="card-icon">
            <i class="material-icons">account_box</i>
        </div>
        <h4 class="card-title">
            <?=$model->isNewRecord ? \Yii::t('app', 'Select Membership Type') : \Yii::t('app', 'Update Membership Type')?>
            <div class="pull-right">
                <?= Html::a(Html::tag('b', 'keyboard_arrow_left', ['class' => 'material-icons']) , ['index'], [
                    'class' => 'btn btn-xs btn-danger btn-round btn-fab',
                    'rel'=>"tooltip",
                    'data' => [
                        'placement' => 'bottom',
                        'original-title' => 'Back'
                    ],
                ]) ?>
            </div>
        </h4>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => "{input} {error}",
            ]
        ]); ?>
        <div class="row">
            <div class="col-sm-12">
            <?= $form->field($model, 'user_id')->hiddenInput(['value'=>$user->id])->label(false) ?>

            <?= $form->field($model, 'status')->hiddenInput(['value'=>'Review'])->label(false) ?>

            <?= $form->field($model, 'membership_type_id')->dropDownList(ArrayHelper::map($types, 'id','name'),['prompt'=>'Select Membership Type','class'=>'browser-default custom-select']) ?>

            </div>
        </div>
    </div>
    <div class="card-footer ml-auto mr-auto">
        <?= Html::submitButton('Save', ['class' => 'btn btn-danger']) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
    </div>
</div>
