<?php

namespace app\modules\member;

/**
 * member module definition class
 */
class Member extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\member\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->layout = '/dashboard/main.php';
        \Yii::$app->errorHandler->errorAction = 'member/default/error';
        // custom initialization code goes here
    }
}
