<?php

namespace app\modules\member\controllers;

use yii\web\Controller;
use yii\web\UploadedFile;
use app\models\Events;
use app\models\Licenses;
use yii\data\ActiveDataProvider;
use app\models\Applications;
use app\models\Payments;


/**
 * Default controller for the `member` module
 */
class ProfileController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $user = \app\models\Users::findOne(\Yii::$app->user->id);
        return $this->render('index',[
            'user' => $user,
        ]);
    }

    public function actionDash()
    {
        $time = new \DateTime('now');
        $today = $time->format('Y-m-d');
        $eventsCount = Events::find()->Where(['>=', 'end_date', $today])->count();

        $upcoming_events = Events::find()->Where(['>=', 'end_date', $today])->limit(8)->all();

        // Get Latest License

        $license = Licenses::find()->where(['user_id'=>\Yii::$app->user->id])->orderBy('id desc')->limit(1)->one(); 
        
        $applications = Applications::find()->where(['user_id'=>\Yii::$app->user->id])->orderBy('id desc'); 
        $dataProvider = new ActiveDataProvider([
            'query' => $applications,
            'pagination'=>[
                'pageSize'=>5,
            ],
        ]);

        $paymentsTotal = Payments::find()->where(['user_id'=>\Yii::$app->user->id])->sum('amount');
        return $this->render('dash',[
            'eventsCount' => $eventsCount,
            'upcoming_events' => $upcoming_events,
            'license' => $license,
            'dataProvider' => $dataProvider,
            'paymentsTotal' => $paymentsTotal,
        ]);
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        if ($user->load(\Yii::$app->request->post()) ) {
            $user->attachment_image = UploadedFile::getInstance($user, 'attachment_image');
            if($user->attachment_image){
                $attachmentPath = time().$user->attachment_image->baseName . '.' . $user->attachment_image->extension;
                $user->attachment = $attachmentPath;
            }
            $user->photo_image = UploadedFile::getInstance($user, 'photo_image');
            if($user->photo_image){
                $photoPath = time().$user->photo_image->baseName . '.' . $user->photo_image->extension;
                $user->photo = $photoPath;
            }
            if($user->save()){
                if($user->attachment_image){
                    $user->attachment_image->saveAs(\Yii::getAlias('@webroot') .'/profiles/' . $attachmentPath);
                }
                if($user->photo_image){
                    $user->photo_image->saveAs(\Yii::getAlias('@webroot') .'/photos/' . $photoPath);
                }
                $notification = new \app\models\Notifications;
                $notification->user_id = \Yii::$app->user->id;
                $notification->subject = 'Profile Details';
                $message = 'You have updated your profile details successfully! Go ahead and apply for membership!';
                $notification->message = $message;
                $notification->save();
                \Yii::$app->session->setFlash('success', $message);
                return $this->redirect(['index']);
            }else{
                $errors = $user->getFirstErrors();
                $message = join(', ',$errors);
                print_r($message);  
                \Yii::$app->session->setFlash('error', $message);
            }
        }
        return $this->render('update', [
            'user' => $user,
        ]);
    }

    protected function findModel($id)
    {
        if (($user = \app\models\Users::findOne($id)) !== null) {
            return $user;
        }

        throw new NotFoundHttpException('The requested profile does not exist.');
    }

}
