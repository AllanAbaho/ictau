<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\Payments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Payments::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Payments();
        $application = \app\models\Applications::findOne($id);

        $alreadyPaid = Payments::find()
        ->where(['application_id'=>$id])->limit(1)->one();
        if($alreadyPaid){
            Yii::$app->session->setFlash('error','You already made a payment that is pending approval!');
            return $this->redirect(['applications/view','id'=>$id]);
        }

        if ($model->load(Yii::$app->request->post()) ) {
            $model->receipt = UploadedFile::getInstance($model, 'receipt');
            if($model->receipt){
                $photoPath = time().$model->receipt->baseName . '.' . $model->receipt->extension;
                $model->file = $photoPath;
                $model->user_id = \Yii::$app->user->id;
                $model->application_id = $id;
                if($model->save()){
                    $application->status = $model->status;
                    $application->save(false);
                    $model->receipt->saveAs(Yii::getAlias('@webroot') .'/receipts/' . $photoPath);
                    Yii::$app->session->setFlash('success','Payment submitted successfully and is pending approval!');
                    return $this->redirect(['applications/view','id'=>$id]);        
                }
            }
            return $this->render('create', [
                'model' => $model,
                'application' => $application,
            ]);    
        }

        return $this->render('create', [
            'model' => $model,
            'application' => $application,
        ]);
    }

    public function actionOlycash($id)
    {
        $application = \app\models\Applications::findOne($id);

        
        $alreadyPaid = Payments::find()
        ->where(['application_id'=>$id, 'user_id'=>\Yii::$app->user->id])->limit(1)->one();
        if($alreadyPaid){
            Yii::$app->session->setFlash('error','You already made a payment that is pending approval!');
            return $this->redirect(['applications/view','id'=>$id]);
        }

        return $this->render('olycash', [
            'application' => $application,
        ]);
    }

    public function actionConfirm($id){
        // Yii::$app->controller->enableCsrfValidation = false;
        $application = \app\models\Applications::findOne($id);
        
        $purchase_id = $_POST['purchase_id'];
        $message = $_POST['message'];
        $payment_type = $_POST['payment_type'];
        if($message == 'success'){
            try {
                $model = new Payments();
                $model->user_id = \Yii::$app->user->id;
                $model->application_id = $id;
                $model->reference_number = $purchase_id;
                $model->payment_type = $payment_type;
                $model->amount = $application->membership->fee;
                $model->name =$application->user->name;
                $model->phone_number = $application->user->primary_phone_number;
                $model->status = 'Paid';
                if($model->save()){
                    $license = new \app\models\Licenses();
                    $license->user_id = $application->user_id;
                    $license->application_id = $id;
                    $license->start_date = date('Y-m-d');
                    $license->end_date = date("Y-m-d", strtotime("+1 year", strtotime($license->start_date)));
                    $license->save();
                    $application->status = 'Complete';
                    $application->save();
                    $notification = new \app\models\Notifications;
                    $notification->user_id = $model->user_id;
                    $notification->subject = 'Download Certificate';
                    $message = 'Your payment has been received and your membership certificate has been issued to you accordingly! Please head to the Certificates tab to download it!';
                    $notification->message = $message;
                    $notification->save();
                    Yii::$app->session->setFlash('success',$message);
                    return $this->redirect(['/member/licenses/index']);
                }else{
                    print_r($model->getFirstErrors());
                }
            } catch (IntegrityException $e) {
                echo $e->getMessage();
            } catch (NotFoundHttpException $e) {
                echo $e->getMessage();
            }
            return $this->redirect(['/member/payments/index']);
        }

    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
