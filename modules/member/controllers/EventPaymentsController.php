<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\EventPayments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventPaymentsController implements the CRUD actions for EventPayments model.
 */
class EventPaymentsController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventPayments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EventPayments::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventPayments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EventPayments();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionOlycash($id)
    {
        $event = \app\models\Events::findOne($id);
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        $alreadyPaid = EventPayments::find()
        ->where(['event_id'=>$id, 'user_id'=>$user->id])->limit(1)->one();
        if($alreadyPaid){
            Yii::$app->session->setFlash('error','You already made a payment for this event!');
            return $this->redirect(['events/view','id'=>$id]);
        }

        return $this->render('olycash', [
            'event' => $event,
            'user' => $user,
        ]);
    }
    public function actionConfirmAttendance($id)
    {
        $event = \app\models\Events::findOne($id);
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        $alreadyPaid = EventPayments::find()
        ->where(['event_id'=>$id, 'user_id'=>$user->id])->limit(1)->one();
        try {
            $model = new EventPayments();
            $model->user_id = \Yii::$app->user->id;
            $model->event_id = $id;
            $model->name =$user->name;
            $model->phone_number = $user->primary_phone_number;
            $model->status = 'Free';
            if($model->save()){
                $notification = new \app\models\Notifications;
                $notification->user_id = $model->user_id;
                $notification->subject = 'Download Ticket';
                $message = 'Your ticket is being processed, Please refresh page after a minute to download it!';
                $notification->message = $message;
                $notification->save();
                Yii::$app->session->setFlash('success',$message);
                return $this->redirect(['/member/events/index']);
            }else{
                print_r($model->getFirstErrors());
            }
        } catch (IntegrityException $e) {
            echo $e->getMessage();
        } catch (NotFoundHttpException $e) {
            echo $e->getMessage();
        }
        return $this->redirect(['/member/event-payments/index']);

    }

    public function actionConfirm($id){
        // Yii::$app->controller->enableCsrfValidation = false;
        $event = \app\models\Events::findOne($id);
        $user = \app\models\Users::findOne(\Yii::$app->user->id);
        $purchase_id = $_POST['purchase_id'];
        $message = $_POST['message'];
        $payment_type = $_POST['payment_type'];
        if($message == 'success'){
            try {
                $model = new EventPayments();
                $model->user_id = \Yii::$app->user->id;
                $model->event_id = $id;
                $model->reference_number = $purchase_id;
                $model->payment_type ='Olycash';
                $model->amount = $event->members_cost;
                $model->name =$user->name;
                $model->phone_number = $user->primary_phone_number;
                $model->status = 'Paid';
                if($model->save()){
                    $notification = new \app\models\Notifications;
                    $notification->user_id = $model->user_id;
                    $notification->subject = 'Download Ticket';
                    $message = 'Your ticket is being processed, Please refresh page after a minute to download it!';
                    $notification->message = $message;
                    $notification->save();
                    Yii::$app->session->setFlash('success',$message);
                    return $this->redirect(['/member/events/index']);
                }else{
                    print_r($model->getFirstErrors());
                }
            } catch (IntegrityException $e) {
                echo $e->getMessage();
            } catch (NotFoundHttpException $e) {
                echo $e->getMessage();
            }
            return $this->redirect(['/member/event-payments/index']);
        }

    }


    /**
     * Updates an existing EventPayments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EventPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EventPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventPayments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
