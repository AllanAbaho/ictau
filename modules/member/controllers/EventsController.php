<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\Events;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\EventPayments;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Events::find()->where(['>=','end_date', date('Y-m-d')]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPastEvents()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Events::find()->where(['<=','end_date', date('Y-m-d')]),
        ]);

        return $this->render('past-events', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $user = \app\models\Users::findOne(\Yii::$app->user->id);
        $alreadyPaid = EventPayments::find()
        ->where(['event_id'=>$id, 'user_id'=>$user->id])->limit(1)->one();

        $payment = \app\models\EventPayments::findOne(['event_id'=>$id,'user_id'=>\Yii::$app->user->id]);
        $feedback = \app\models\EventFeedback::findOne(['event_id'=>$id,'user_id'=>\Yii::$app->user->id]);
        if(!$feedback){
            $feedback = new \app\models\EventFeedback();
        }
        if($feedback->load(Yii::$app->request->post()) && $feedback->save()) {
            Yii::$app->session->setFlash('success','Thanks for giving us this feedback!');
            return $this->redirect(['index']);
        }    

        return $this->render('view', [
            'model' => $this->findModel($id),
            'payment' => $payment,
            'alreadyPaid' => $alreadyPaid,
            'feedback' => $feedback
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApply()
    {
        $model = new \app\models\Applications();
        $model->user_id = \Yii::$app->user->id;
        $model->category = 'Event';
        $model->status = 'Review';
        $model->save();
        return $this->redirect(['/member/payments/olycash-events', 'id' => $model->id]);
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
