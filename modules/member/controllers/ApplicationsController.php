<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\Applications;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApplicationsController implements the CRUD actions for Applications model.
 */
class ApplicationsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Applications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Applications::find()->where(['user_id'=>\Yii::$app->user->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Applications model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Applications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $alreadyMember = Applications::find()
            ->where(['user_id'=>\Yii::$app->user->id,'status'=>'Complete'])
            ->limit(1)->one();
        $alreadyApplying = Applications::find()
            ->where(['user_id'=>\Yii::$app->user->id])
            ->andWhere(['not',['status'=>['Complete','Rejected']]])
            ->limit(1)->one();
        if($alreadyMember){
            Yii::$app->session->setFlash('error','You are already a member!');
            return $this->redirect(['index']);
        }
        if($alreadyApplying){
            Yii::$app->session->setFlash('error','You already have an application being processed!');
            return $this->redirect(['index']);
        }
        $model = new Applications();
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'user' => $user,            
        ]);
    }

    public function actionChange()
    {
        $alreadyApplying = Applications::find()
            ->where(['user_id'=>\Yii::$app->user->id])
            ->andWhere(['not',['status'=>'Complete']])
            ->limit(1)->one();
        if($alreadyApplying){
            Yii::$app->session->setFlash('error','You already have an application being processed!');
            return $this->redirect(['index']);
        }
        $model = new Applications();
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('change', [
            'model' => $model,
            'user' => $user,            
        ]);
    }

    /**
     * Updates an existing Applications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,            
        ]);
    }


    public function actionSubmitApplication($id)
    {
        $model = $this->findModel($id);
        $model->status = 'Pending Approval';
        $model->save();
        $notification = new \app\models\Notifications;
        $admin = \app\models\Users::findOne(['role'=>'Admin']);
        $notification->user_id = $admin->id;
        $notification->subject = 'New Application Submitted';
        $message = 'There has been a new application submitted on the system and it is pending your approval!';
        $notification->message = $message;
        $notification->save();
        Yii::$app->session->setFlash('success','Your application will be reviewed shortly!');
        return $this->redirect(['index']);
    }

    public function actionRenewMembership()
    {
        $alreadyApplying = Applications::find()
        ->where(['user_id'=>\Yii::$app->user->id])
        ->andWhere(['not',['status'=>'Complete']])
        ->limit(1)->one();
        if($alreadyApplying){
            Yii::$app->session->setFlash('error','You already have an application being processed!');
            return $this->redirect(['index']);
        }

        $model = new Applications();
        $user = \app\models\Users::findOne(\Yii::$app->user->id);

        $model->status = 'Pending Payment';
        $model->user_id = $user->id;
        $model->membership_type_id = $user->license->application->membership->id;
        if ($model->save()) {
            $notification = new \app\models\Notifications;
            $admin = \app\models\Users::findOne(['role'=>'Admin']);
            $notification->user_id = $admin->id;
            $notification->subject = 'New Application Submitted';
            $message = 'There has been a new application submitted on the system and it is pending payment!';
            $notification->message = $message;
            $notification->save();
            Yii::$app->session->setFlash('success','Please make payment and attach the receipt!');
            return $this->redirect(['index']);    
        }
    }

    /**
     * Deletes an existing Applications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Applications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Applications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Applications::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
