<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ictau_members".
 *
 * @property int $id
 * @property string $name
 * @property string|null $email_address
 * @property string|null $primary_phone_number
 * @property string|null $license_end_date
 */
class IctauMembers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organisations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['license_end_date'], 'safe'],
            [['name', 'email_address', 'primary_phone_number'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email_address' => 'Email Address',
            'primary_phone_number' => 'Primary Phone Number',
            'license_end_date' => 'License End Date',
        ];
    }
}
