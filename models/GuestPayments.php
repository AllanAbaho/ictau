<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;

/**
 * This is the model class for table "guest_payments".
 *
 * @property int $id
 * @property string|null $payment_type
 * @property int|null $event_id
 * @property string|null $amount
 * @property string|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone_number
 * @property string|null $reference_number
 * @property string|null $file
 */
class GuestPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guest_event_payments';
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone_number'], 'required'],
            [['event_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_type', 'amount', 'status', 'name', 'email', 'phone_number', 'reference_number', 'file','receipt'], 'string', 'max' => 100],
            [['email'],'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_type' => 'Payment Type',
            'event_id' => 'Event ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'email' => 'Email',
            'phone_number' => 'Phone Number',
            'reference_number' => 'Reference Number',
            'file' => 'Ticket',
            'receipt' => 'Receipt',
        ];
    }

    public function getEvent(){
        return Events::findOne($this->event_id);
    }

    public function getName(){
        return $this->name;
    }
}
