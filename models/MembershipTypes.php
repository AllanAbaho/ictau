<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "membership_types".
 *
 * @property int $id
 * @property string $name
 * @property string|null $fee
 * @property string|null $template
 * @property int|null $updated_by
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class MembershipTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'membership_types';
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['template'], 'string'],
            [['updated_by', 'created_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'fee'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'fee' => 'Fee',
            'template' => 'Template',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
