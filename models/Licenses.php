<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "licenses".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $application_id
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string|null $updated_at
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property int|null $created_by
 */
class Licenses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'licenses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'application_id', 'updated_by', 'created_by'], 'integer'],
            [['start_date', 'end_date', 'updated_at', 'created_at','file'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'application_id' => 'Application ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
        ];
    }

    public function getUser(){
        return $this->hasOne(Users::classname(), ['id' => 'user_id']);
    }

    public function getApplication(){
        return $this->hasOne(Applications::classname(), ['id' => 'application_id']);
    }

}
