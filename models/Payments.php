<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $payment_type
 * @property int|null $application_id
 * @property string|null $amount
 * @property string|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $name
 * @property string|null $phone_number
 */
class Payments extends \yii\db\ActiveRecord
{
    public $receipt;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_payments';
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'application_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_type', 'amount', 'status', 'name', 'phone_number','reference_number','file'], 'string', 'max' => 100],
            [['receipt'],'file', 'skipOnEmpty'=>true, 'extensions' => 'pdf, png, jpg, jpeg', 
            'maxSize'=>1024*1024*2, 'mimeTypes'=> 'application/pdf, image/jpeg, image/png',
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'payment_type' => 'Payment Type',
            'application_id' => 'Application ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'name' => 'Name',
            'phone_number' => 'Phone Number',
        ];
    }

    public function getUser(){
        return $this->hasOne(Users::classname(), ['id' => 'user_id']);
    }

    public function getApplication(){
        return $this->hasOne(Applications::classname(), ['id' => 'application_id']);
    }

}
