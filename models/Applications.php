<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $status
 * @property string|null $reason
 * @property int|null $event_id
 * @property int|null $membership_type_id
 * @property int|null $updated_by
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property string|null $created_at
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'updated_by', 'created_by','membership_type_id'], 'integer'],
            [['reason'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['status'], 'string', 'max' => 100],
        ];
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'membership_type_id' => 'Membership Type',
            'status' => 'Status',
            'reason' => 'Reason',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getUser(){
        return $this->hasOne(Users::classname(), ['id' => 'user_id']);
    }

    public function getPayment(){
        return $this->hasOne(Payments::classname(), ['application_id' => 'id']);
    }

    public function getMembership(){
        return MembershipTypes::findOne($this->membership_type_id);
    }

    public function getMemberhip(){
        return $this->hasOne(MembershipTypes::classname(), ['id' => 'membership_type_id']);
    }

}
