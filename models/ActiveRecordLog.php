<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "active_record_log".
 *
 * @property integer $id
 * @property string $description
 * @property string $action
 * @property string $model
 * @property integer $model_id
 * @property string $field
 * @property string $created_at
 * @property integer $userid
 */
class ActiveRecordLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'active_record_log';
    }

    public function behaviors()
    {
        return [

            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'userid'], 'integer'],
            [['created_at'], 'safe'],
            [['description', 'action', 'model'], 'string', 'max' => 45],
            [['field'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'action' => 'Action',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'field' => 'Field',
            'created_at' => 'Created At',
            'userid' => 'User',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(),['id'=>'userid']);
    }

}
